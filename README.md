# AtomicsWorld API

Die AWAPI ist ein Muss für jeden Server, denn ohne sie kann kein Server laufen. Sie bietet vielseitige Funktionen für die Entwickler und 

### Installation

1. Zieh das Plugin auf den Server in den /plugins Ordner
2. Erstelle einen AtomicsWorld Ordner
3. Schreibe folgende Inhalte dort hinein (die MySQL-Daten bitte auch eintragen):

```sh
mysql:
  host:
  port:
  user:
  password:
  database:
joinmessagetype: actionbar
access:
  enabled: false
  permission: builder
  message: &4Halt Stop!
```

Sollten die MySQL-Daten fehlerhaft sein, kann der Server nicht gestartet werden!

### Anleitung für Entwickler
http://31.214.240.137/gitlab/AtomicsWorld/AWAPI/wikis/api