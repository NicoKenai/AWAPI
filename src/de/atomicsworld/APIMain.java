package de.atomicsworld;

import com.avaje.ebean.EbeanServer;
import com.google.common.collect.Sets;
import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import de.atomicsworld.database.AWDatabase;
import de.atomicsworld.database.DatabaseConfiguration;
import de.atomicsworld.functions.*;
import de.atomicsworld.functions.ban.BanCommands;
import de.atomicsworld.functions.AWCommand;
import de.atomicsworld.functions.ban.BanListener;
import de.atomicsworld.functions.permissions.Perms;
import de.atomicsworld.listener.*;
import de.atomicsworld.redis.AWRedis;
import de.atomicsworld.redis.AWRedisPoolConfig;
import de.atomicsworld.util.Stats;
import de.atomicsworld.util.dispatching.Dispatcher;
import de.atomicsworld.util.dispatching.ResourceBundle;
import de.atomicsworld.util.executor.types.BukkitExecutor;
import de.atomicsworld.util.executor.ThreadExecutor;
import de.atomicsworld.util.menu.PopupMenuAPI;
import de.atomicsworld.util.slack.SlackApi;
import de.atomicsworld.util.slack.SlackChannel;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.bukkit.Bukkit;
import org.bukkit.configuration.MemorySection;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by NicoH on 05.12.2016.
 */
public class APIMain extends JavaPlugin {

    public static EbeanServer database;
    public static Dispatcher dsp;
    public static String prefix = "§3AtomicsWorld§8 » §7";
    public static String symbol = "§7»";
    public static String noperm = prefix + "§cDafür hast du keine Berechtigung!";
    public static String usage = prefix + "§7Falsche Nutzung!\n§8» §7";
    public static Plugin plugin;
    public static APIMain instance;
    public static SlackApi sysadmin = new SlackApi("https://hooks.slack.com/services/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");
    public static SlackApi logs = new SlackApi("https://hooks.slack.com/services/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");
    public static ServerData data = new ServerData();

    @Override
    public void onLoad() {
        if (instance != null) instance = this;
        ThreadExecutor.setExecutor(new BukkitExecutor());
        initializePlugin();
        loadDatabase();
        connectToRedis();
        loadMessageKeys();
        dsp = new Dispatcher(this);
    }

    @Override
    public void onEnable() {
        registerFunctions();
        new Tablist();
        registerListener();
        loadAPI();
        if (database == null) Bukkit.getServer().shutdown();
        AWAPI.sendSlackMessage(SlackChannel.SYSADMIN, "Starte Server *" + Bukkit.getServerName() + "*..");

        ThreadExecutor.executeLater(() -> data.create(), 5);
    }

    @Override
    public void onDisable() {
        data.remove();
        AWAPI.sendSlackMessage(SlackChannel.SYSADMIN, "Stoppe Server *" + Bukkit.getServerName() + "*..");


        AWRedis.disable();
    }

    public static void sendServerInfo() {
        data.update();
    }

    public void registerFunctions() {
        AWCommand.setPluginInstance(this);
        AWCommand.add("reloadconfig", new Reloadconfig());
        AWCommand.add("nick", new Nick());
        AWCommand.add("createserver", new Createserver());
        AWCommand.add("help", new Help());
        AWCommand.add("atomics", new Atomics());
        AWCommand.add("playerinfo", new Playerinfo());
        AWCommand.add("access", new Access());
        AWCommand.add("colors", new Colors());
        AWCommand.add("gamemode", new Gamemode());
        AWCommand.add("togglechat", new Togglechat());
        AWCommand.add("shutdown", new Shutdown());
        AWCommand.add("chat", new ChatSystem());
        AWCommand.add("onlinetop", new Onlinetop());

        AWAPI.register(Playersettings.class, this);
        AWAPI.register(Clock.class, this);
        AWAPI.register(Pay.class, this);
        AWAPI.register(Perms.class, this);
        AWAPI.register(Redeem.class, this);
        AWAPI.register(Stats.class, this);
        AWAPI.register(Test.class, this);
        AWAPI.register(NewsSystem.class, this);
        AWAPI.register(BanCommands.class, this);
        AWAPI.register(YoutuberManager.class, this);
    }

    public void registerListener() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new JoinListener(), this);
        pm.registerEvents(new UserAcceptor(), this);
        pm.registerEvents(new ChatListener(), this);
        pm.registerEvents(new BanListener(), this);
        pm.registerEvents(new Access(), this);
        pm.registerEvents(new QuitListener(), this);
        pm.registerEvents(new ServerListener(), this);
        pm.registerEvents(new Nick(), this);
        pm.registerEvents(new PopupMenuAPI(), this); //MenuAPI
    }

    public void loadAPI() {
        AWAPI.loadApi(this, getClassLoader(), database);
    }

    public void initializePlugin() {
        plugin = this;
    }

    public void loadDatabase() {
        try {
            database = AWDatabase.getServer(new DatabaseConfiguration((MemorySection) plugin.getConfig().get("mysql")));
            System.out.println("[AtomicsWorld] Successfully connected to the database via Ebean at AWAPI.");
        } catch (Exception ex) {
            System.err.println("[AtomicsWorld] Error while trying to connect to the database via Ebean at AWAPI");
            System.out.println("| " + ex.getMessage());
            Throwable t = ex.getCause();
            while (t != null) {
                System.out.println("| " + t.getMessage());
                t = t.getCause();
            }
        }
    }

    public void loadMessageKeys() {
        ResourceBundle.loadResourceBundle("api");
        System.out.println("[AtomicsWorld] Successfully loaded messages file.");
    }

    public void connectToRedis() {
        MemorySection section = (MemorySection) getConfig().get("redis");
        AWRedisPoolConfig config = AWRedis.prepareConfig();
        config.setMaster(section.getString("master"));
        config.setHosts(Sets.newHashSet(section.getStringList("hosts")));
        config.setPassword(section.getString("password"));
        AWRedis.initialize(config);
        System.out.println("[AtomicsWorld] Successfully connected to Redis Server.");

        {
            GenericObjectPoolConfig poolConfig = config.getPoolConfig();
            poolConfig.setMaxTotal(section.getInt("maxTotal", 200));
            poolConfig.setMinIdle(section.getInt("minIdle", 1));
            poolConfig.setMaxIdle(section.getInt("maxIdle", 8));
        }
        AWRedis.getSubscriptionHandler().run();
    }
}