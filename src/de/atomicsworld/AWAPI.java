package de.atomicsworld;


import com.avaje.ebean.EbeanServer;
import com.google.common.base.Joiner;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import de.atomicsworld.database.entities.Server;
import de.atomicsworld.database.entities.Statistics;
import de.atomicsworld.database.entities.User;
import de.atomicsworld.database.entities.UserSettings;
import de.atomicsworld.functions.Atomics;
import de.atomicsworld.redis.AWRedis;
import de.atomicsworld.util.TypeUtils;
import de.atomicsworld.util.command.AtomicsCommand;
import de.atomicsworld.util.command.BukkitCommand;
import de.atomicsworld.util.executor.ThreadExecutor;
import de.atomicsworld.util.slack.SlackChannel;
import de.atomicsworld.util.slack.SlackMessage;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

import static de.atomicsworld.APIMain.dsp;

/**
 * Created by NicoH on 05.12.2016.
 */
public class AWAPI {

    private static Plugin plugin;
    private static ClassLoader classLoader;
    public static EbeanServer db;

    public static EbeanServer getDatabase() {
        return db;
    }

    public static void loadApi(Plugin pl, ClassLoader cl, EbeanServer database) {
        plugin = pl;
        classLoader = cl;
        db = database;
    }

    public static User getUser(Integer user_id) {
        return db.find(User.class).where().eq("id", user_id).findUnique();
    }

    public static User getUser(String name) {
        return db.find(User.class).where().eq("user_name", name).findUnique();
    }

    public static User getUser(Player p) {
        return db.find(User.class).where().eq("uuid", p.getUniqueId()).findUnique();
    }

    public static User getUser(UUID uuid) {
        return db.find(User.class).where().eq("uuid", uuid).findUnique();
    }

    public static User getUser(OfflinePlayer p) {
        return db.find(User.class).where().eq("uuid", p.getUniqueId()).findUnique();
    }

    public static boolean isNicked(UUID uuid) {
        User u = getUser(uuid);
        return u.getNickname() != null;
    }

    public static String getNick(UUID uuid) {
        return getUser(uuid).getNickname();
    }

    public static void addAtomics(UUID uuid, double add) {
        ThreadExecutor.executeAsync(() -> {
            User u = db.find(User.class).where().eq("uuid", uuid).findUnique();
            u.setAtomics(u.getAtomics() + add);
            db.update(u);
        });
    }

    public static void removeAtomics(UUID uuid, double remove) {
        ThreadExecutor.executeAsync(() -> {
            User u = db.find(User.class).where().eq("uuid", uuid).findUnique();
            u.setAtomics(u.getAtomics() - remove);
            db.update(u);
        });
    }

    public static void sendPlayer(Player p, String server) {
        AWRedis.publish("send.player", p.getUniqueId() + ";" + server);
    }

    public static void sendPlayerToHub(Player p) {
        sendPlayer(p, "lobby01");
    }

    public static Server getServer(String name) {
        return db.find(Server.class).where().eq("servername", name).findUnique();
    }

    public static void setServerState(Server server, Server.ServerState status) {
        ThreadExecutor.executeAsync(() -> {
            Server s = server;
            s.setState(status);
            db.update(s);
        });
        AWRedis.execute(jedis -> {
            String name = server.getServername();
            if (jedis.hexists("server:" + name, "ingame")) {
                jedis.hdel("server:" + name, "ingame");
            }
            if (status == Server.ServerState.INGAME) {
                jedis.hset("server:" + name, "ingame", Objects.toString(true));
            } else jedis.hset("server:" + name, "ingame", Objects.toString(false));
            return true;
        });
    }

    public static void setPlayerSetting(UUID uuid, String title, String value) {
        ThreadExecutor.executeAsync(() -> {
            if (getSetting(uuid, title) != null) {
                UserSettings settings = db.find(UserSettings.class).where().eq("uuid", uuid).eq("title", title).findUnique();
                settings.setValue(value);
                db.update(settings);
            } else {
                UserSettings settings;
                if (value == null) {
                    settings = new UserSettings(uuid, title);
                } else settings = new UserSettings(uuid, title, value);
                db.save(settings);
            }
        });

    }

    public static void removePlayerSetting(UUID uuid, String title) {
        ThreadExecutor.executeAsync(() -> db.delete(getSetting(uuid, title)));
    }

    public static String getPlayerString(UUID uuid, String title) {
        return getSetting(uuid, title).getValue().toString();
    }

    public static Integer getPlayerInteger(UUID uuid, String title) {
        return Integer.parseInt(getSetting(uuid, title).getValue());
    }

    public static Boolean getPlayerBoolean(UUID uuid, String title) {
        return Boolean.parseBoolean(getSetting(uuid, title).getValue());
    }

    public static UserSettings getSetting(UUID uuid, String title) {
        return db.find(UserSettings.class).where().eq("uuid", uuid).eq("title", title).findUnique();
    }

    public static List<UserSettings> getSettings(UUID uuid) {
        return db.find(UserSettings.class).where().eq("uuid", uuid).findList();
    }

    public static void sendSlackMessage(SlackChannel channel, String message) {
        if (message.contains("dev")) return;
        channel.getApi().call(new SlackMessage(message).setIcon(":loudspeaker:").setUsername(channel.getName()));
    }

    public static void register(Class functionClass, JavaPlugin plugin) {
        try {
            register(functionClass, functionClass.newInstance(), plugin);
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Registriert Listener und Commands aus einer Klasse für ein Plugin
     *
     * @param function Object der Klasse welche registriert werden soll
     * @param plugin   Plugin für welches die Klasse registriert wird
     */
    public static void register(Object function, JavaPlugin plugin) {
        register(function.getClass(), function, plugin);
    }

    /**
     * Registriert Listener und Commands aus einer Klasse für ein Plugin
     *
     * @param functionClass Klasse welche registriert werden soll
     * @param function      Object der Klasse welche registriert werden soll
     * @param plugin        Plugin für welches die Klasse registriert wird
     */
    public static void register(Class functionClass, Object function, JavaPlugin plugin) {
        Method[] methods = functionClass.getDeclaredMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(AtomicsCommand.class))
                registerCommand(function, method, plugin);
        }

        if (function instanceof Listener) {
            Bukkit.getPluginManager().registerEvents((Listener) function, plugin);
        }
    }

    private static Map<String, Command> commandMap = new HashMap<>();
    private static List<Object[]> unavailableSubcommands = new ArrayList<>();

    private static void registerCommand(Object function, Method method, JavaPlugin plugin) {
        AtomicsCommand cmd = method.getAnnotation(AtomicsCommand.class);

        if (cmd.parent().length == 0) {
            BukkitCommand tBukkitCommand = new BukkitCommand(plugin, function, method, cmd);
            tBukkitCommand.register();

            commandMap.put(tBukkitCommand.getName(), tBukkitCommand);

            for (Object[] unavailableSubcommand : unavailableSubcommands) {
                Method oldMethod = (Method) unavailableSubcommand[1];
                AtomicsCommand old = oldMethod.getAnnotation(AtomicsCommand.class);
                if (old.parent()[0].equalsIgnoreCase(cmd.name()))
                    registerCommand(unavailableSubcommand[0], oldMethod, plugin);
            }
        } else {
            Command pluginCommand = commandMap.get(cmd.parent()[0]);
            if (pluginCommand == null) {
                unavailableSubcommands.add(new Object[]{function, method});
                Joiner.on(" ").join(cmd.parent() + " " + cmd.name(), cmd.parent()[0]);
            } else {
                if (pluginCommand instanceof BukkitCommand) {
                    ((BukkitCommand) pluginCommand).getProcessor().addSubCommand(cmd, function, method);
                } else {
                    Joiner.on(" ").join(cmd.parent() + " " + cmd.name(), cmd.parent()[0]);
                }
            }
        }
    }

    public static void changeStatsEntry(String key, Object value) {
        String var0 = value.toString();
        if (getStatsEntry(key) != null) {
            Statistics statistics = getStatsEntry(key);
            statistics.setValue(var0);
            db.update(statistics);
        } else {
            Statistics statistics = new Statistics(key, var0);
            db.save(statistics);
        }
    }

    public static Statistics getStatsEntry(String key) {
        return db.find(Statistics.class).where().eq("key", key).findUnique();
    }

    public static void addGameToStats() {
        changeStatsEntry("games", TypeUtils.toInt(getStatsEntry("games").getValue()) + 1);
    }
}