package de.atomicsworld.database;

import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.config.DataSourceConfig;
import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import de.atomicsworld.database.entities.*;
import net.minecraft.server.v1_11_R1.Statistic;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nico on 12.10.2016.
 */

public class AWDatabase {

    private static EbeanServer database = AWAPI.db;

    public static EbeanServer getDatabase() {
        return database;
    }

    public static EbeanServer getServer(IDatabaseConfiguration configuration) {
        DataSourceConfig mysqlDb = new DataSourceConfig();
        mysqlDb.setDriver("com.mysql.jdbc.Driver");
        mysqlDb.setUsername(configuration.getUsername());
        mysqlDb.setPassword(configuration.getPassword());
        mysqlDb.setUrl(String.format("jdbc:mysql://%s:%s/%s", configuration.getHostname(), configuration.getPort(), configuration.getDatabase()));
        mysqlDb.setHeartbeatSql("SELECT 1 FROM dual");
        DatabaseHandler database = new DatabaseHandler(APIMain.plugin.getClass().getClassLoader()) {
            protected java.util.List<Class<?>> getDatabaseClasses() {
                List<Class<?>> list = new ArrayList<>();
                list.add(Bans.class);
                list.add(User.class);
                list.add(Server.class);
                list.add(UserSettings.class);
                list.add(Locations.class);
                list.add(BungeePerms.class);
                list.add(Redeem.class);
                list.add(Statistics.class);
                list.add(News.class);
                list.add(SuperSteveStats.class);
                return list;
            }
        };
        database.initializeDatabase(mysqlDb.getDriver(), mysqlDb.getUrl(), mysqlDb.getUsername(), mysqlDb.getPassword(), "SERIALIZABLE", false, false);
        return database.getDatabase();
    }
}