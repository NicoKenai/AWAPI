package de.atomicsworld.database;

import org.bukkit.configuration.MemorySection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nico on 12.10.2016.
 */
public class DatabaseConfiguration implements IDatabaseConfiguration {

    private MemorySection config;

    public DatabaseConfiguration(MemorySection config) {
            this.config = config;
        }
        @Override
        public String getHostname() {
            return config.getString("host");
        }
        @Override
        public int getPort() {
            return config.getInt("port");
        }
        @Override
        public String getUsername() {
            return config.getString("user");
        }
        @Override
        public String getPassword() {
            return config.getString("password");
        }
        @Override
        public String getDatabase() {
            return config.getString("database");
        }
        @Override
        public List<Class> getAdditionalClasses() {
            return new ArrayList<>();
        }
        @Override
        public void addClasses(Class... classes) {
            throw new RuntimeException("Not supported");
        }
        @Override
        public int getMaxConnections() {
            return config.getInt("maxConnections", 20);
        }
}
