package de.atomicsworld.database;

import java.util.List;

/**
 * Created by Nico on 12.10.2016.
 */
public interface IDatabaseConfiguration {
    String getHostname();
    int getPort();
    String getUsername();
    String getPassword();
    String getDatabase();
    List<Class> getAdditionalClasses();
    void addClasses(Class... add);
    int getMaxConnections();
}
