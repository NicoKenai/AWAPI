package de.atomicsworld.database.entities;

import com.avaje.ebean.validation.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.UUID;

/**
 * Created by NicoH on 05.12.2016.
 */
@Entity()
@Table(name = "aw_bans")
public class Bans {

    @Id
    public int id;

    @NotNull
    public String uuid;

    @NotNull
    public String reason;

    @NotNull
    @Column(name = "authorUuid")
    public String authorUuid;

    @NotNull
    @Column(name = "bannedDate")
    public long bannedDate;

    @NotNull
    public long duration;

    @NotNull
    @Column(name = "bannedUntil")
    public long bannedUntil;

    public Bans(){}

    public Bans(String uuid, String reason, String authorUuid, long bannedDate, long duration, long bannedUntil){
        setBannedUntil(bannedUntil);
        setDuration(duration);
        setUuid(uuid);
        setReason(reason);
        setAuthorUuid(authorUuid);
        setBannedDate(bannedDate);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public long getBannedUntil() {
        return bannedUntil;
    }

    public String getAuthorUuid() {
        return authorUuid;
    }

    public long getDuration() {
        return duration;
    }

    public String getReason() {
        return reason;
    }

    public String getUuid() {
        return uuid;
    }

    public long getBannedDate() {
        return bannedDate;
    }

    public void setAuthorUuid(String authorUuid) {
        this.authorUuid = authorUuid;
    }

    public void setBannedDate(long bannedDate) {
        this.bannedDate = bannedDate;
    }

    public void setBannedUntil(long bannedUntil) {
        this.bannedUntil = bannedUntil;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}

