package de.atomicsworld.database.entities;

import com.avaje.ebean.validation.NotNull;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bungeeperms_permissions2")
public class BungeePerms {

    @Id
    public int id;
    @NotNull
    public String name;
    @NotNull
    public int type;
    @NotNull
    public String key;
    @NotNull
    public String value;
    public String server;
    public String world;

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String getServer() {
        return server;
    }

    public String getWorld() {
        return world;
    }

    public String getValue() {
        return value;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setWorld(String world) {
        this.world = world;
    }

    public void setId(int id) {
        this.id = id;
    }
}
