package de.atomicsworld.database.entities;

import com.avaje.ebean.validation.NotNull;
import org.bukkit.Location;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import static org.bukkit.Bukkit.getServer;

/**
 * Created by Kenai on 05.02.2017 at 12:04.
 */
@Entity()
@Table(name = "aw_locations")
public class Locations {

    @Id
    public int id;

    @NotNull
    public String value;

    @NotNull
    public String location;

    public void setLocation(Location location) {
        this.location = loc2str(location);
    }

    public Location getLocation() {
        return str2loc(location);
    }

    public String getName() {
        return value;
    }

    public void setName(String name) {
        this.value = name;
    }


    public Location str2loc(String str) {
        String str2loc[] = str.split("\\:");
        Location loc = new Location(getServer().getWorld(str2loc[0]), 0, 0, 0);
        loc.setX(Double.parseDouble(str2loc[1]));
        loc.setY(Double.parseDouble(str2loc[2]));
        loc.setZ(Double.parseDouble(str2loc[3]));
        return loc;

    }


    public String loc2str(Location loc) {
        return loc.getWorld().getName() + ":" + loc.getBlockX() + ":" + loc.getBlockY() + ":" + loc.getBlockZ();
    }


}
