package de.atomicsworld.database.entities;

import com.avaje.ebean.validation.NotNull;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Table(name = "aw_news")
public class News {

    @Id
    public int id;

    @NotNull
    public UUID creator;

    @NotNull
    public Timestamp createdDate;

    @NotNull
    public String title;

    @NotNull
    public String news;

    public News(){

    }

    public News(UUID creator, Timestamp createdDate, String title, String news){
        setCreator(creator);
        setCreatedDate(createdDate);
        setTitle(title);
        setNews(news);
    }

    public String getNews() {
        return news;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setNews(String news) {
        this.news = news;
    }

    public int getId() {
        return id;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public UUID getCreator() {
        return creator;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public void setCreator(UUID creator) {
        this.creator = creator;
    }

    public void setId(int id) {
        this.id = id;
    }
}
