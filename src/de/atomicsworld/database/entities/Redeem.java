package de.atomicsworld.database.entities;

import com.avaje.ebean.validation.NotNull;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "aw_redeem")
public class Redeem  {

    @Id
    public int id;

    @NotNull
    public String name;

    @NotNull
    public String code;

    @NotNull
    public String creater;

    @NotNull
    public Timestamp createDate;

    @NotNull
    public boolean used;

    public String bukkitConsoleAction;

    public String bungeeConsoleAction;

    public String playerExecuteAction;

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBukkitConsoleAction() {
        return bukkitConsoleAction;
    }

    public String getCode() {
        return code;
    }

    public String getBungeeConsoleAction() {
        return bungeeConsoleAction;
    }

    public String getCreater() {
        return creater;
    }

    public String getName() {
        return name;
    }

    public String getPlayerExecuteAction() {
        return playerExecuteAction;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setBukkitConsoleAction(String bukkitConsoleAction) {
        this.bukkitConsoleAction = bukkitConsoleAction;
    }

    public void setBungeeConsoleAction(String bungeeConsoleAction) {
        this.bungeeConsoleAction = bungeeConsoleAction;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public void setPlayerExecuteAction(String playerExecuteAction) {
        this.playerExecuteAction = playerExecuteAction;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }
}

