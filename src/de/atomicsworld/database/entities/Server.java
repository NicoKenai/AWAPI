package de.atomicsworld.database.entities;

import com.avaje.ebean.validation.NotNull;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Kenai on 08.01.2017 at 20:05.
 */
@Entity()
@Table(name = "aw_server")
public class Server {

    @Id
    public int id;

    @NotNull
    public String servername;

    @NotNull
    public ServerState state;


    @NotNull
    public int maxplayer;

    @NotNull
    public int port;

    public Type type;

    public Server() {
    }

    public int getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getPort() {
        return port;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setMaxplayer(int maxplayer) {
        this.maxplayer = maxplayer;
    }

    public int getMaxplayer() {
        return maxplayer;
    }

    public ServerState getState() {
        return state;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServername() {
        return servername;
    }

    public void setServername(String servername) {
        this.servername = servername;
    }

    public void setState(ServerState state) {
        this.state = state;
    }

    public enum Type {
        GAME,
        DEFAULT;
    }

    public enum ServerState {
        NORMAL, //if it isnt a game (eg a lobby)
        WAITING,
        STARTING,
        INGAME,
        END,
        OFFLINE;
    }

}
