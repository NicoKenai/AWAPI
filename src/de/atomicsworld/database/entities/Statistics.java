package de.atomicsworld.database.entities;

import com.avaje.ebean.validation.NotNull;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Datenbank Tabelle für sämtliche Statistiken, wie viel gespielt wurde etc
 */
@Entity
@Table(name = "aw_statistics")
public class Statistics {

    @Id
    public int id;

    @NotNull
    public String key;

    @NotNull
    public String value;

    public Statistics(){}

    public Statistics(String key, String value){
        setValue(value);
        setKey(key);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
