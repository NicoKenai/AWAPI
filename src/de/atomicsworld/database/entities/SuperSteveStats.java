package de.atomicsworld.database.entities;

import com.avaje.ebean.validation.NotNull;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Deprecated
@Entity()
@Table(name = "aw_supersteve_stats")
public class SuperSteveStats {

    @Id
    public int id;

    @NotNull
    public int userId;

    @NotNull
    public int points;

    @NotNull
    public int world;

    @NotNull
    public int level;

    public SuperSteveStats(){}

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getLevel() {
        return level;
    }

    public int getPoints() {
        return points;
    }

    public int getUserId() {
        return userId;
    }

    public int getWorld() {
        return world;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setWorld(int world) {
        this.world = world;
    }
}
