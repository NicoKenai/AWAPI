package de.atomicsworld.database.entities;

import com.avaje.ebean.validation.NotNull;
import de.atomicsworld.util.ItemStackBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.UUID;

/**
 * Created by Niico on 31.12.2016.
 */
@Entity()
@Table(name = "aw_user")
public class User {
    @Id
    public int userId;

    @NotNull
    public String userName;

    @NotNull
    public UUID uuid;

    @NotNull
    public double atomics;

    public long playtime;

    public int kicked;

    public String nickname;

    @NotNull
    public Timestamp firstJoin;

    public Timestamp lastJoin;

    public boolean lastNewsRead;

    public User(){}

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public double getAtomics() {
        return atomics;
    }

    public long getPlaytime() {
        return playtime;
    }

    public void setPlaytime(long playtime) {
        this.playtime = playtime;
    }

    public String getUserName() {
        return userName;
    }

    public UUID getUuid() {
        return uuid;
    }

    public int getKicked() {
        return kicked;
    }

    public void setKicked(int kicked) {
        this.kicked = kicked;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Timestamp getFirstJoin() {
        return firstJoin;
    }

    public Timestamp getLastJoin() {
        return lastJoin;
    }

    public void setAtomics(double atomics) {
        this.atomics = atomics;
    }

    public void setFirstJoin(Timestamp firstJoin) {
        this.firstJoin = firstJoin;
    }

    public void setLastJoin(Timestamp lastJoin) {
        this.lastJoin = lastJoin;
    }

    public boolean isLastNewsRead() {
        return lastNewsRead;
    }

    public void setLastNewsRead(boolean lastNewsRead) {
        this.lastNewsRead = lastNewsRead;
    }
}
