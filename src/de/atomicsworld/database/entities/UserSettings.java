package de.atomicsworld.database.entities;

import com.avaje.ebean.validation.NotNull;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

/**
 * Created by Kenai on 18.01.2017 at 15:11.
 */
@Entity()
@Table(name = "aw_user_settings")
public class UserSettings {

    @Id
    public int id;

    @NotNull
    public UUID uuid;

    @NotNull
    public String title;

    public String value;

    public UserSettings(){}

    public UserSettings(UUID uuid, String title, String value){
        setUuid(uuid);
        setTitle(title);
        setValue(value);
    }

    public UserSettings(UUID uuid, String title){
        this(uuid, title, null);
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getValue() {
        return value;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
