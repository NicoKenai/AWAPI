package de.atomicsworld.functions;

import de.atomicsworld.APIMain;
import de.atomicsworld.util.executor.ThreadExecutor;
import javafx.beans.DefaultProperty;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;

import static de.atomicsworld.APIMain.noperm;

/**
 * Use AtomicsCommand (@interface annotation) please!
 */
@Deprecated
public abstract class AWCommand implements CommandExecutor {

    protected static final String PERMISSION_PREFIX = "aw.";

    private String name;
    private String permission = "";
    private boolean ingame = false;
    private int minArgs = 0;
    private int maxArgs = -1;
    protected String usage;
    private boolean runAsync = false;

    public static final void add(String commandName, AWCommand awCommand) {
        PluginCommand cmd = APIMain.instance.getCommand(commandName);
        awCommand.name = commandName;
        awCommand.usage = cmd.getUsage();
        awCommand.prepare();
        cmd.setExecutor(awCommand);
    }

    public static final void setPluginInstance(APIMain instance) {
        APIMain.instance = instance;
    }

    protected abstract void prepare();

    public abstract void perform(final CommandSender sender, String[] args);

    public final boolean onCommand(final CommandSender sender, final Command cmd, final String label, String[] args) {
        if (!permission.equalsIgnoreCase("") && !sender.hasPermission(this.permission)) {
            sender.sendMessage(noperm);
            return true;
        }

        if (ingame && !(sender instanceof Player)) {
            System.out.println("The console cannot execute that command!");
            return false;
        }

        if (args.length >= minArgs && (args.length <= maxArgs || maxArgs == -1))
            if (runAsync) {
                ThreadExecutor.executeAsync(()-> perform(sender, args));
            } else perform(sender, args);
        else
            sender.sendMessage(APIMain.usage + this.usage);

        return true;
    }

    protected final void permission() {
        this.permission(this.PERMISSION_PREFIX + this.name);
    }

    protected final void permission(String permission) {
        this.permission = PERMISSION_PREFIX + permission;
    }

    protected final void ingame() {
        this.ingame = true;
    }

    protected final void minArgs(int minArgs) {
        this.minArgs = minArgs;
    }

    protected final void maxArgs(int maxArgs) {
        this.maxArgs = maxArgs;
    }

    protected final void runAsync() {
        this.runAsync = true;
    }

}
