package de.atomicsworld.functions;

import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import static de.atomicsworld.APIMain.prefix;
import static de.atomicsworld.APIMain.symbol;

/**
 * Created by Niico on 31.12.2016.
 */
public class Access extends AWCommand implements Listener {

    private static String[] permissions = new String[]{"admin", "dev", "mod", "sup", "builder", "helfer", "youtuber", "premium"};

    @Override
    protected void prepare() {
        permission("access.manage");
        usage = "/access <allow|restrict (-k)|info|setpermission|setmsg> [value]";
        minArgs(1);
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        if (args[0].equalsIgnoreCase("allow")) {
            if (isAccess()) {
                disable();
                sender.sendMessage(prefix + "Du hast die Whitelist ausgestellt.");
            } else sender.sendMessage(prefix + "Die Whitelist ist nicht aktiviert.");
        }

        if (args[0].equalsIgnoreCase("restrict")) {
            if (!(isAccess())) {
                activate();
                for (String s : args) {
                    if (s.contains("-k")) {
                        Bukkit.getOnlinePlayers().forEach(o -> {
                            if (!o.hasPermission(APIMain.instance.getConfig().getString("access.permission")))
                                o.sendMessage(prefix + "§cDie Whitelist wurde aktiviert und du hast keine Berechtigung mehr, auf diesem Server zu sein! §7"
                                        + symbol + "§c Du bist nun auf der Lobby.");
                            AWAPI.sendPlayerToHub(o);
                        });
                        sender.sendMessage(prefix + "Die Whitelist wurde aktiviert!\n"
                                + symbol + "Alle Spieler, dessen Berechtigungen nicht ausreichen, wurden vom Server gekickt.");
                    }
                }
                sender.sendMessage(prefix + "Du hast die Whitelist aktiviert.");
            } else sender.sendMessage(prefix + "Die Whitelist ist bereits aktiviert.");
        }

        if (args[0].equalsIgnoreCase("info")) {
            StringBuilder access_info = new StringBuilder();
            if (!isAccess()) {
                access_info.append(prefix + "Aktuell ist §ckeine§7 Whitelist aktiviert.\n");
            } else access_info.append(symbol + "Die Whitelist ist aktuell §aaktiviet§7.\n");
            access_info.append(symbol + "Permission: §e" + APIMain.instance.getConfig().getString("access.permission"));
            sender.sendMessage(access_info.toString());
        }

        if (args[0].equalsIgnoreCase("setmsg")) {
            String msg = "";
            for (int i = 1; i < args.length; i++) msg += args[i] + " ";
            sender.sendMessage(prefix + "Whitelist-Nachricht umgeändert!\n" + symbol + "§7 Vorher: §e" +
                    ChatColor.translateAlternateColorCodes('&',  APIMain.instance.getConfig().getString("access.message")) + "\n§7 Nachher: §e" +
                    ChatColor.translateAlternateColorCodes('&', msg));
            setMessage(msg);
        }

        if (args[0].equalsIgnoreCase("setpermission")) {
            String permission = args[1];
            boolean x = false;
            for (String s : permissions) {
                if (permission.contains(s)) {
                    setPermission(permission);
                    sender.sendMessage(prefix + "Whitelist-Permission umgeändert!\n" + symbol + " Vorher: §e" + APIMain.instance.getConfig().getString("access.permission") + "\n§7" + symbol + " Nachher:§e " + permission);
                    x = true;
                    break;
                }
            }
            if (!x) sender.sendMessage(prefix + "Ungültiger Wert (muss der Name eines Rangs sein)!");
        }
    }

    @EventHandler
    public void onLoginWhileAccess(PlayerLoginEvent e) {
        if (isAccess()) {
            if (e.getPlayer().hasPermission(APIMain.instance.getConfig().getString("access.permission")))
                e.allow();
            else
                e.disallow(PlayerLoginEvent.Result.KICK_WHITELIST, prefix + ChatColor.translateAlternateColorCodes('&',  APIMain.instance.getConfig().getString("access.message")));
        }
    }

    private static void disable() {
        APIMain.instance.getConfig().set("access.enabled", false);
        saveConfig();
    }

    private static void activate() {
        APIMain.instance.getConfig().set("access.enabled", true);
        saveConfig();
    }

    private static void saveConfig() {
        APIMain.instance.saveConfig();
        APIMain.instance.reloadConfig();
    }

    private static void setMessage(String message) {
        APIMain.instance.getConfig().set("access.message", message);
        saveConfig();
    }

    private static void setPermission(String permission) {
        APIMain.instance.getConfig().set("access.permission", "aw.color." + permission);
        saveConfig();
    }

    public static boolean isAccess() {
        return APIMain.instance.getConfig().getBoolean("access.enabled");
    }

}
