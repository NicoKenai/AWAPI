package de.atomicsworld.functions;

import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import de.atomicsworld.database.entities.User;
import de.atomicsworld.functions.AWCommand;
import de.atomicsworld.util.TypeUtils;
import de.atomicsworld.util.command.AtomicsCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static de.atomicsworld.APIMain.*;

/**
 * Created by Niico on 01.01.2017.
 */
public class Atomics extends AWCommand {

    String COMMAND_PERMISSION = "atomics"; //aw.atomics
    String VIEW_OTHER = "aw.atomics.view.other";
    String CHANGE = "aw.atomics.change";

    @Override
    protected void prepare() {
        permission(COMMAND_PERMISSION);
        usage = "/atomics [Player] (Change)";
        maxArgs(3);
        minArgs(0);
        ingame();
        runAsync();
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        Player p = (Player) sender;
        User u = AWAPI.getUser(p);
        if (args.length == 0) {
            APIMain.database.refresh(u);
            dsp.send(sender, "atomics.info", (int) u.getAtomics());
        }

        if (args.length == 1) {
            if (sender.hasPermission(VIEW_OTHER)) {
                User u2 = AWAPI.getUser(args[0]);
                if (u2 != null) {
                    APIMain.database.refresh(u2);
                    dsp.send(sender, "atomics.info.other", u2.getUserName(), (int) u2.getAtomics());
                } else dsp.playerNull(sender, args[0]);
            } else sender.sendMessage(noperm);
        }

        if (args.length == 2) {
            if (sender.hasPermission(CHANGE)) {
                User u3 = AWAPI.getUser(args[0]);
                if (TypeUtils.isInt(args[1])) {
                    int change = Integer.parseInt(args[1]);
                    AWAPI.addAtomics(u3.getUuid(), change);
                    APIMain.database.update(u3);
                    dsp.send(sender, "atomics.transaction.success", u3.getUserName(), u3.getAtomics() + change, args[1]);
                } else sender.sendMessage(prefix + usage);
            } else sender.sendMessage(noperm);
        }
    }
}
