package de.atomicsworld.functions;

import de.atomicsworld.APIMain;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ChatSystem extends AWCommand {

    public ChatSystem() {

    }

    @Override // /chat
    protected void prepare() {
        permission("chat.other");
        usage = "/chat <Spieler> <Nachricht>";
        minArgs(2);
    }

    static String reason = "";
    @Override
    public void perform(CommandSender sender, String[] args) {
        for (int i = 1; i < args.length; i++) reason += " " + args[i];
        Player target = Bukkit.getPlayer(args[0]);
        if (target.isOnline()) {
            sender.sendMessage(APIMain.prefix + "Nachricht für " + target.getName() + "§7 geschrieben.");
            Bukkit.getOnlinePlayers().forEach(p -> p.sendMessage(target.getDisplayName() + "§8»§7" + reason));
            reason = "";
        } else sender.sendMessage(APIMain.prefix + "§cDer Spieler existiert nicht.");
    }

}