package de.atomicsworld.functions;

import de.atomicsworld.util.command.AtomicsCommand;
import org.bukkit.command.CommandSender;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import static de.atomicsworld.APIMain.dsp;

/**
 * Created by Niico on 31.12.2016.
 */
public class Clock {

    @AtomicsCommand(
            name = "clock",
            maxArgs = 0,
            usage = "/clock",
            aliases = "time"
    )
    public void onClock(CommandSender sender){
        dsp.send(sender, "time.info", SimpleDateFormat.getInstance().format(new java.sql.Date(new Timestamp(System.currentTimeMillis()).getTime())));
    }
}
