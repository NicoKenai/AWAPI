package de.atomicsworld.functions;

import de.atomicsworld.functions.AWCommand;
import org.bukkit.command.CommandSender;

import static de.atomicsworld.APIMain.prefix;
import static de.atomicsworld.APIMain.symbol;

/**
 * Created by Niico on 31.12.2016.
 */
public class Colors extends AWCommand {

    @Override
    protected void prepare() {
        permission("chat.color");
        maxArgs(0);
        usage = "/colors";
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(prefix + "Farbcodes:\n");
        stringBuilder.append(symbol + "0 - §0Schwarz§r\n");
        stringBuilder.append(symbol + "§71 - §1Blau§r\n");
        stringBuilder.append(symbol + "§72 - §2Grün§r\n");
        stringBuilder.append(symbol + "§73 - §3Dunkeltürkis§r\n");
        stringBuilder.append(symbol + "§74 - §4Rot§r\n");
        stringBuilder.append(symbol + "§75 - §5Lila§r\n");
        stringBuilder.append(symbol + "§76 - §6Orange§r\n");
        stringBuilder.append(symbol + "§77 - Grau§r\n");
        stringBuilder.append(symbol + "§78 - §8Grau\n");
        stringBuilder.append(symbol + "§79 - §9Dunkelblau§r\n");
        stringBuilder.append(symbol + "§7a - §aHellgrün§r\n");
        stringBuilder.append(symbol + "§7b - §bTürkis§r\n");
        stringBuilder.append(symbol + "§7c - §cHellrot§r\n");
        stringBuilder.append(symbol + "§7d - §dPink§r\n");
        stringBuilder.append(symbol + "§7e - §eGelb§r\n");
        stringBuilder.append(symbol + "§7r - §rWeiß§r\n");
        stringBuilder.append(symbol + "§7l - §lFett§r\n");
        stringBuilder.append(symbol + "§r§7n - §nUnterstrichen§r\n");
        stringBuilder.append(symbol + "§r§7o - §oKurisv§r");
        sender.sendMessage(stringBuilder.toString());
    }
}
