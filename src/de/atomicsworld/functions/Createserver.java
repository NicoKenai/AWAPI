package de.atomicsworld.functions;

import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import de.atomicsworld.database.entities.Server;
import org.bukkit.command.CommandSender;

import static de.atomicsworld.APIMain.dsp;


/**
 * Created by Kenai on 08.01.2017 at 20:29.
 */
public class Createserver extends AWCommand {

    @Override
    protected void prepare() {
        permission("cloudadmin");
        usage = "/createserver <name> <maxplayer> <port>";
        maxArgs(3);
        minArgs(3);
        runAsync();
    }

    @Override
    public void perform(CommandSender sender, String[] args) {

        String name = args[0];
        int max = Integer.parseInt(args[1]);
        int port = Integer.parseInt(args[2]);

        if(AWAPI.getServer(name) != null){
            dsp.send(sender, "server.exists");
            return;
        }

        Server s = new Server();
        s.setServername(name);
        s.setMaxplayer(max);
        s.setPort(port);
        s.setType(null);
        s.setState(Server.ServerState.OFFLINE);
        APIMain.database.save(s);

        dsp.send(sender, "server.created");
    }
}
