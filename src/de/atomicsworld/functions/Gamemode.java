package de.atomicsworld.functions;

import de.atomicsworld.functions.AWCommand;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static de.atomicsworld.APIMain.dsp;

/**
 * Created by NicoH on 05.12.2016.
 */
public class Gamemode extends AWCommand {

    @Override
    protected void prepare() {
        minArgs(0);
        maxArgs(1);
        permission("gamemode");
        usage = "/gamemode [Player]";
        ingame();
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        if(args.length == 1){
            if(Bukkit.getPlayer(args[0]) != null && Bukkit.getPlayer(args[0]).isOnline()){
                Player x = Bukkit.getPlayer(args[0]);
                if(!(x.getGameMode().equals(GameMode.CREATIVE))){
                    x.setGameMode(GameMode.CREATIVE);
                    dsp.send(x, "gamemode.changed", x.getGameMode().name());
                    dsp.send(sender, "gamemode.changed.other", x.getName(), x.getGameMode().name());
                } else {
                    x.setGameMode(GameMode.SURVIVAL);
                    dsp.send(x, "gamemode.changed", x.getGameMode().name());
                    dsp.send(sender, "gamemode.changed.other", x.getName(), x.getGameMode().name());
                }
            } else dsp.playerNull(sender, args[0]);

        } else {
            Player x = (Player) sender;
            if(!(x.getGameMode().equals(GameMode.CREATIVE))){
                x.setGameMode(GameMode.CREATIVE);
                dsp.send(x, "gamemode.changed", x.getGameMode().name());
                return;
            } else x.setGameMode(GameMode.SURVIVAL);
            dsp.send(x, "gamemode.changed", x.getGameMode().name());
        }
    }
}
