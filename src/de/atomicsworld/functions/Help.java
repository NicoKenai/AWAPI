package de.atomicsworld.functions;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import static de.atomicsworld.APIMain.prefix;
import static de.atomicsworld.APIMain.symbol;

/**
 * Created by Niico on 01.01.2017.
 */
public class Help extends AWCommand {

    StringBuilder help = new StringBuilder();

    @Override
    protected void prepare() {
        permission("help");
        maxArgs(0);
        usage = "/help";
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        StringBuilder help = new StringBuilder();
        help.append(prefix + "§7§lHilfsübersicht:§r");

        add("atomics", "Zeigt deine aktuellen Atomics");
        add("pay", "Überweist Atomics");
        add("clear", "Leert dein Inventar");
        add("colors", "Zeigt alle verfügbaren Farben");
        add("playerinfo", "Zeigt Spielerinformationen");

        sender.sendMessage(help.toString());
    }

    private void add(String command, String usage) {
        help.append("\n" + symbol + "§e/" + command + "§8 |§7 " + usage + ChatColor.RESET);
    }
}