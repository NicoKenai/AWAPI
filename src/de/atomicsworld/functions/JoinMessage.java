package de.atomicsworld.functions;

import de.atomicsworld.APIMain;
import de.atomicsworld.util.Actionbar;
import de.atomicsworld.util.RankColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class JoinMessage {

    public enum Action {
        JOIN,
        LEAVE
    }

    public static void makeAnnoucement(Action action, Player p) {
        String t = APIMain.plugin.getConfig().getString("joinmessagetype");
        String name = p.getDisplayName();

        switch (action) {
            case LEAVE:
                if (t.equalsIgnoreCase("chat"))
                    Bukkit.getOnlinePlayers().forEach(op -> op.sendMessage("§3» §8[§4-§8] " + RankColor.get(p) + p.getName()));
                 else if (t.equalsIgnoreCase("actionbar")) Actionbar.online("§3» §8[§4-§8] " + name);
                break;
            case JOIN:
                if (t.equalsIgnoreCase("chat"))
                    Bukkit.getOnlinePlayers().forEach(op -> op.sendMessage("§3» §8[§2+§8] " + RankColor.get(p) + p.getName()));
                 else if (t.equalsIgnoreCase("actionbar")) Actionbar.online("§3» §8[§2+§8] " + name);
                break;
            default:
                return;
        }
    }

}
