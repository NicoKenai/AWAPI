package de.atomicsworld.functions;

import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import de.atomicsworld.database.entities.User;
import de.atomicsworld.util.UUIDFetcher;
import de.atomicsworld.util.executor.ThreadExecutor;
import de.atomicsworld.util.menu.MenuItem;
import de.atomicsworld.util.menu.PopupMenu;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.text.SimpleDateFormat;

public class NewsMenu_Dashboard extends PopupMenu {

    int i = 0;

    public NewsMenu_Dashboard(Player viewer) {
        super("Neuigkeiten", 6);
        ThreadExecutor.executeAsync(() -> {

            User u = AWAPI.getUser(viewer);
            if (!u.isLastNewsRead()) {
                u.setLastNewsRead(true);
                AWAPI.getDatabase().update(u);
            }

            NewsSystem.getNews().forEach(news -> {
                this.addMenuItem(new MenuItem("§6" + news.getTitle(), new ItemStack(Material.PAPER), "§7vom §e" + SimpleDateFormat.getInstance().format(new java.sql.Date(news.getCreatedDate().getTime())) + " §7(von §e" + UUIDFetcher.getName(news.getCreator()) + "§7)") {
                    @Override
                    public void onClick(Player player) {
                        closeMenu(player);
                        player.sendMessage(ChatColor.GRAY + news.getNews().replaceAll("}", ""));
                    }
                }, i++);
            });
        }).onDone(() -> {
            openMenu(viewer);
            i = 0;
        });
    }

}
