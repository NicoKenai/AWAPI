package de.atomicsworld.functions;

import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import de.atomicsworld.database.entities.News;
import de.atomicsworld.database.entities.User;
import de.atomicsworld.util.TimeUtils;
import de.atomicsworld.util.UUIDFetcher;
import de.atomicsworld.util.command.AtomicsCommand;
import de.atomicsworld.util.executor.ThreadExecutor;
import de.atomicsworld.util.slack.SlackChannel;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.HashMap;
import java.util.List;

public class NewsSystem implements Listener {

    public static HashMap<Player, String> title = new HashMap<>();
    public static HashMap<Player, String> text = new HashMap<>();


    @AtomicsCommand(
            name = "news",
            usage = "/news",
            maxArgs = 0,
            minArgs = 0,
            runAsync = true,
            noConsole = true
    )
    public void news(Player p) {
        new NewsMenu_Dashboard(p);
    }

    @AtomicsCommand(
            name = "cancel",
            parent = "news",
            usage = "/news cancel",
            permissions = {"aw.news.write"}
    )
    public void cancel(Player p) {
        title.remove(p);
        text.remove(p);
        p.sendMessage(APIMain.prefix + "News-Schreib Vorgang abgebrochen.");
    }

    @AtomicsCommand(
            name = "delete",
            parent = "news",
            minArgs = 1,
            usage = "/news delete <Titel>",
            permissions = {"aw.news.write"},
            runAsync = true
    )
    public void delete(Player p, String title) {
        getNews().forEach(news -> {
            if(news.getTitle().contains(title)){
               AWAPI.getDatabase().delete(news);
               p.sendMessage(APIMain.prefix + "Die News mit dem Titel §e"+news.getTitle()+"§7 von "+ UUIDFetcher.getName(news.getCreator())+" wurde gelöscht.");
            }
        });
    }

    @AtomicsCommand(
            name = "write",
            parent = "news",
            usage = "/news write <Titel>"
    )
    public void write(Player p, String titel) {
        if (titel == null) {
            p.sendMessage(APIMain.prefix + "Du musst einen Titel angeben.");
            return;
        }
        p.sendMessage(APIMain.prefix + "Der Titel wurde gespeichert.\n§8" + APIMain.symbol + "§7 " +
                "(Mit §e/news cancel§7 brichst du den Vorgang ab)\n§8" + APIMain.symbol + "§7 " +
                "Mit §e\"&\" §7kannst du farbig schreiben.\n§8" + APIMain.symbol + "§7 " +
                "Es wird eine neue Zeile mit einem \"§e;\"§7 generiert.\n§8" + APIMain.symbol + "§7 " +
                "Wenn die Nachricht ein \"§e}\"§7 beinhaltet, wird die News abgeschickt.");
        title.put(p, titel);
        text.put(p, "");
    }

    public static void send(Player p) {
        p.sendMessage(APIMain.prefix + "Versende News...");
        ThreadExecutor.executeAsync(() -> {

            News news = new News(p.getUniqueId(), TimeUtils.getCurrentTimestamp(), title.get(p), text.get(p));
            APIMain.database.save(news);

            APIMain.database.find(User.class).findList().forEach(user -> {
                user.setLastNewsRead(false);
                APIMain.database.update(user);
            });
            title.remove(p);
            text.remove(p);
            AWAPI.sendSlackMessage(SlackChannel.LOGS, "*" + p.getName() + "* hat eine News verfasst! Titel: _" + title.get(p) + "_\n\n " + text.get(p));
        }).onDone(() -> {
            p.sendMessage(APIMain.prefix + "Die News wurde erfolgreich versandt.");
        });
    }

    public static List<News> getNews() {
        return APIMain.database.find(News.class).order().desc("created_date").setMaxRows(63 + 1).findList();
    }
}