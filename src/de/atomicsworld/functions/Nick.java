package de.atomicsworld.functions;

import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import de.atomicsworld.database.entities.User;
import de.atomicsworld.util.RankColor;
import de.atomicsworld.util.executor.ThreadExecutor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static de.atomicsworld.APIMain.dsp;
import static de.atomicsworld.APIMain.instance;

/**
 * Created by Kenai on 09.01.2017.
 */
public class Nick extends AWCommand implements Listener {

    private static ArrayList<String> nicks = new ArrayList<>();

    public static void setNicks() { //http://appamatix.com/1616-cool-minecraft-names-usernames/
        nicks.add("Hawkb");
        nicks.add("TheCampingRusher");
        nicks.add("Numbers");
        nicks.add("cookie10");
        nicks.add("Vikkstar123HD");
        nicks.add("AmazingHuh");
        nicks.add("immortalHD");
        nicks.add("ItsKyuhl");
        nicks.add("odinlevi");
        nicks.add("macejoe");
        nicks.add("cj1124");
        nicks.add("cj1124");
        nicks.add("Baconlovar");
        nicks.add("CthulhuToo");
        nicks.add("Blood");
        nicks.add("nilyak");
        nicks.add("Aquaticthickshell");
        nicks.add("supersandvich");
        nicks.add("xViperLink");
        nicks.add("aranamor");
        nicks.add("TheNerdHerd");
        nicks.add("Country");
        nicks.add("Hideki");
        nicks.add("CursedPing");
        nicks.add("jbean99");
        nicks.add("TNDPTanster");
        nicks.add("PvPDucks");
        nicks.add("chiefyy");
        nicks.add("chaz200");
        nicks.add("PatClone");
        nicks.add("HuckLetsPlay");
    }

    public static void unnick(Player p) {
        ThreadExecutor.executeAsync(() -> {
            User u = AWAPI.getUser(p);
            u.setNickname(null);
            AWAPI.db.update(u);
            if (u.getNickname() != null) nicks.add(u.getNickname());

            p.setDisplayName(RankColor.get(p).replaceAll(RankColor.get(p).substring(0, 1), "") + p.getName());
            p.setPlayerListName(RankColor.get(p).replaceAll(RankColor.get(p).substring(0, 1), "") + p.getName());
        });
    }


    public static void nick(Player p, String nick) {
        ThreadExecutor.executeAsync(() -> {
            p.setDisplayName("§9§7" + nick);
            p.setPlayerListName("§9§7" + nick);
            p.setCustomName("§9§7" + nick);
            p.setCustomNameVisible(true);

            User u = AWAPI.getUser(p);
            u.setNickname(nick);
            AWAPI.db.update(u);
        });
    }


    private boolean validNick(String nick) {
        List<User> list = APIMain.database.find(User.class).where().eq("nickname", nick).findList();
        return list.isEmpty();
    }

    @Override
    protected void prepare() {
        permission("nick");
        maxArgs(0);
        usage = "/nick";
        runAsync();
        ingame();
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        if (nicks.isEmpty()) setNicks();
        Player p = (Player) sender;
        if (AWAPI.isNicked(p.getUniqueId())) {
            unnick((Player) sender);
            dsp.send(sender, "nick.unnick");
            return;
        }

        Random r = new Random();
        String nick = nicks.get(r.nextInt(nicks.size()));
        if (validNick(nick)) {
            nick((Player) sender, nick);
            dsp.send(sender, "nick.success", nick);
        } else dsp.send(sender, "nick.error");
    }
}
