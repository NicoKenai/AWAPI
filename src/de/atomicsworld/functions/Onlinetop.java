package de.atomicsworld.functions;

import de.atomicsworld.APIMain;
import de.atomicsworld.database.entities.User;
import de.atomicsworld.util.TimeUtils;
import org.bukkit.command.CommandSender;

import java.util.List;

public class Onlinetop extends AWCommand {

    @Override
    protected void prepare() {
        permission("onlinetop");
        maxArgs(0);
        runAsync();
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        APIMain.database.find(User.class).order().desc("playtime").setMaxRows(10).findList().forEach(u -> sender.sendMessage("§6" + u.getUserName() + " §7(§e" + TimeUtils.formatTime(u.getPlaytime()) + "§7)"));
    }
}
