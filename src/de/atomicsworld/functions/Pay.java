package de.atomicsworld.functions;

import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import de.atomicsworld.database.entities.User;
import de.atomicsworld.util.command.AtomicsCommand;
import org.bukkit.entity.Player;

import static de.atomicsworld.APIMain.dsp;

/**
 * Created by Kenai on 14.01.2017.
 */
public class Pay {

    @AtomicsCommand(
            name = "pay",
            maxArgs = 2,
            minArgs = 2,
            noConsole = true,
            runAsync = true,
            usage = "/pay <Spieler> <Betrag>"
    )
    public void pay(Player p, String target, Integer amount) {
        User u3 = AWAPI.getUser(target);
        User u4 = AWAPI.getUser(p);
        if (u3 == null) {
            dsp.playerNull(p, target);
            return;
        }
        if (amount >= 1 && u4.getAtomics() >= amount) {
            AWAPI.addAtomics(u3.getUuid(), amount);
            AWAPI.removeAtomics(u4.getUuid(), amount);
            dsp.send(p, "atomics.pay.success", u3.getUserName(), amount);
            APIMain.database.update(u3);
        } else dsp.send(p, "atomics.pay.tofew");
    }

}
