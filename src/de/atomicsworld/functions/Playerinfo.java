package de.atomicsworld.functions;

import de.atomicsworld.AWAPI;
import de.atomicsworld.database.entities.User;
import de.atomicsworld.functions.AWCommand;
import de.atomicsworld.functions.ban.BanUtils;
import de.atomicsworld.redis.AWRedis;
import de.atomicsworld.util.TimeUtils;
import de.atomicsworld.util.executor.ThreadExecutor;
import org.bukkit.command.CommandSender;

import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import static de.atomicsworld.APIMain.prefix;
import static de.atomicsworld.APIMain.symbol;

/**
 * Created by Niico on 31.12.2016.
 */
public class Playerinfo extends AWCommand {

    private String PERMISSION = "aw.playerinfo.";

    @Override
    protected void prepare() {
        permission("playerinfo");
        maxArgs(1);
        minArgs(1);
        usage = "/playerinfo <Player>";
        runAsync();
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        User u = AWAPI.getUser(args[0]);
        if (u != null) {
            StringBuilder info = new StringBuilder();
            SimpleDateFormat timeFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

            info.append(prefix);
            info.append("Spielerinformationen zu §e" + u.getUserName() + "§7:\n");

            if (sender.hasPermission(PERMISSION + "id"))
                info.append("§7\n" + symbol + " Spieler-ID: §e" + u.getUserId() + "\n");

            if (AWRedis.hget("player:" + u.getUuid().toString(), "server") != null && sender.hasPermission(PERMISSION + "online")) {
                info.append(symbol + " §aOnline§7");
                if (sender.hasPermission(PERMISSION + "online.server"))
                    info.append(" auf §e" + AWRedis.hget("player:" + u.getUuid().toString(), "server"));
                if (sender.hasPermission(PERMISSION + "online.since"))
                    info.append("§7 seit §e" + timeFormat.format(new Date(java.sql.Timestamp.valueOf(AWRedis.hget("player:" + u.getUuid().toString(), "lastjoin")).getTime())));

            } else
                info.append("§7\n" + symbol + " Zuletzt online am §e" + timeFormat.format(new Date(u.getLastJoin().getTime())));

            if (sender.hasPermission(PERMISSION + "firstjoin"))
                info.append("\n§7" + symbol + " Erster Join: §e" + timeFormat.format(new Date(u.getFirstJoin().getTime())));

            if (sender.hasPermission(PERMISSION + "uuid"))
                info.append("\n§7" + symbol + " UUID: §e" + u.getUuid());

            if (sender.hasPermission(PERMISSION + "atomics"))
                info.append("\n§7" + symbol + " Atomics: §e" + (int) u.getAtomics());

            if (sender.hasPermission(PERMISSION + "seenick") && AWAPI.isNicked(u.getUuid())) {
                info.append("\n§7" + symbol + "§7 Nickname: §e" + AWAPI.getNick(u.getUuid()));
            }

            if (sender.hasPermission(PERMISSION + "bans")) {
                Integer size = BanUtils.getBans(u.getUuid()).size();
                if (size > 0) info.append("\n§7" + symbol + " Bans: §e" + BanUtils.getBans(u.getUuid()).size());
            }

            if (sender.hasPermission(PERMISSION + "banned"))
                info.append(BanUtils.isCurrentBanned(u.getUuid()) ? "§7\n" + symbol + "§7 Gebannt bis §c" + timeFormat.format(new Date(BanUtils.getCurrentBanId(u.getUuid()).getBannedUntil())) + " §7(§c#" + BanUtils.getCurrentBanId(u.getUuid()).getId() + "§7)" : "");

            if (sender.hasPermission(PERMISSION + "playtime"))
                info.append("§7\n" + symbol + " Spielzeit: §e" + TimeUtils.formatTime(u.getPlaytime()));

            sender.sendMessage(info.toString());
        } else sender.sendMessage(prefix + "Der Spieler §e" + args[0] + "§7 existiert nicht!");
    }
}