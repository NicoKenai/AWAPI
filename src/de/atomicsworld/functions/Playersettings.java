package de.atomicsworld.functions;

import de.atomicsworld.AWAPI;
import de.atomicsworld.database.entities.User;
import de.atomicsworld.database.entities.UserSettings;
import de.atomicsworld.util.command.AtomicsCommand;
import org.bukkit.command.CommandSender;

import java.util.List;

import static de.atomicsworld.APIMain.dsp;

public class Playersettings {

    @AtomicsCommand(
            name = "playersettings",
            usage = "/playersettings",
            maxArgs = 0
    )
    public void playersettings(CommandSender sender) {
        dsp.send(sender, "playersettings.usage");
    }

    @AtomicsCommand(
            name = "remove",
            parent = "playersettings",
            usage = "/playersettings remove <Player> <Title>",
            minArgs = 2,
            maxArgs = 2,
            runAsync = true
    )
    public void remove(CommandSender sender, String target, String title) {
        User u = AWAPI.getUser(target);
        if (u == null) {
            dsp.playerNull(sender, target);
            return;
        }
        UserSettings settings = AWAPI.getSetting(u.getUuid(), title);
        if (settings == null) {
            dsp.send(sender, "playersettings.remove.null");
            return;
        } else {
            AWAPI.removePlayerSetting(u.getUuid(), title);
            dsp.send(sender, "playersettings.remove.success", title, u.getUserName());
        }
    }

    @AtomicsCommand(
            name = "list",
            parent = "playersettings",
            minArgs = 1,
            maxArgs = 1,
            usage = "/playersettings list <Player>",
            runAsync = true
    )
    public void list(CommandSender sender, String target) {
        User u = AWAPI.getUser(target);
        if (u == null) {
            dsp.playerNull(sender, target);
            return;
        }
        List<UserSettings> list = AWAPI.getSettings(u.getUuid());
        dsp.send(sender, "playersettings.list.title", u.getUserName());
        list.forEach(l -> sender.sendMessage("§8[§e" + l.getTitle() + "§8] §7" + l.getValue()));
    }

    @AtomicsCommand(
            name = "add",
            parent = "playersettings",
            minArgs = 3,
            usage = "/playersettings add <Player> <Title> <Value>",
            runAsync = true
    )
    public void add(CommandSender sender, String target, String title, String value) {
        User u = AWAPI.getUser(target);
        if (u == null) {
            dsp.playerNull(sender, target);
            return;
        }
        AWAPI.setPlayerSetting(u.getUuid(), title, value);
        dsp.send(sender, "playersettings.add.success", title, u.getUserName(), value);
    }
}