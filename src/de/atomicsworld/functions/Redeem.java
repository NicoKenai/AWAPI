package de.atomicsworld.functions;

import de.atomicsworld.AWAPI;
import de.atomicsworld.redis.AWRedis;
import de.atomicsworld.util.UUIDFetcher;
import de.atomicsworld.util.command.AtomicsCommand;
import de.atomicsworld.util.slack.SlackChannel;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;

import static de.atomicsworld.APIMain.database;
import static de.atomicsworld.APIMain.dsp;
import static org.bukkit.Bukkit.getServer;

public class Redeem {

    /**
     * @param all Wenn true, returnt er auch benutze, wenn false nicht.
     * @return Alle existenten Redeem Codes
     */
    public List<de.atomicsworld.database.entities.Redeem> list(boolean all) {
        return database.find(de.atomicsworld.database.entities.Redeem.class).where().eq("used", all).findList();
    }

    public de.atomicsworld.database.entities.Redeem get(String code) {
        return database.find(de.atomicsworld.database.entities.Redeem.class).where().eq("code", code).findUnique();
    }

    public List<de.atomicsworld.database.entities.Redeem> list() {
        return list(false);
    }

    public boolean exists(String code) {
        return get(code) != null;
    }

    public boolean valid(String code) {
        return !get(code).isUsed();
    }


    @AtomicsCommand(
            name = "redeem",
            usage = "/redeem <Code>",
            minArgs = 1,
            maxArgs = 1,
            runAsync = true
    )
    public void onRedeem(Player p, String code) {
        if (exists(code)) {
            if (valid(code)) {
                de.atomicsworld.database.entities.Redeem redeem = get(code);


                if (redeem.getBukkitConsoleAction() != null) {
                    String[] console = redeem.bukkitConsoleAction.split("}");
                    for (String s : console) {
                        getServer().dispatchCommand(Bukkit.getConsoleSender(), s.replaceAll("/", ""));
                    }
                }

                if (redeem.getBungeeConsoleAction() != null) {
                    String[] console = redeem.bungeeConsoleAction.split("}");
                    for (String s : console) {
                        AWRedis.publish("bungee.console", s);
                    }
                }


                if (redeem.getPlayerExecuteAction() != null) {
                    String[] player = redeem.playerExecuteAction.split("}");
                    for (String s : player) {
                        getServer().dispatchCommand(p, s.replaceAll("/", ""));
                    }
                }

                redeem.setUsed(true);
                database.update(redeem);
                dsp.send(p, "redeem.success", redeem.getName() != null ? redeem.getName() : "Viel Spaß!");
                AWAPI.sendSlackMessage(SlackChannel.LOGS, "Der Spieler *" + UUIDFetcher.getName(p.getUniqueId()) + "* hat den Code *_" + redeem.getCode() + "_* eingelöst.");

            } else dsp.send(p, "redeem.used");
        } else dsp.send(p, "redeem.notexists");
    }

}