package de.atomicsworld.functions;

import de.atomicsworld.APIMain;
import de.atomicsworld.functions.AWCommand;
import org.bukkit.command.CommandSender;

import static de.atomicsworld.APIMain.dsp;
import static de.atomicsworld.APIMain.prefix;

/**
 * Created by Kenai on 13.01.2017.
 */
public class Reloadconfig extends AWCommand {

    @Override
    protected void prepare() {
        permission("reload.config");
        maxArgs(0);
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        APIMain.instance.reloadConfig();
        dsp.send(sender, "config.reload");
    }
}
