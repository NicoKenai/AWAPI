package de.atomicsworld.functions;

import de.atomicsworld.redis.AWRedis;
import org.bukkit.Bukkit;

import java.util.Objects;

public class ServerData {
    public static String servername;

    public ServerData() {
        servername = Bukkit.getServerName();
    }

    public void update() {
        AWRedis.execute(jedis -> {
            jedis.hdel("server:" + servername, "player");
            set("player", Objects.toString(Bukkit.getServer().getOnlinePlayers().size()));
            return true;
        });
        publish();
    }

    public void remove() {
        AWRedis.publish("server.remove", servername);
    }

    public void create() {
        set("player", Objects.toString(Bukkit.getOnlinePlayers().size()));
        set("maxplayer", Objects.toString(Bukkit.getMaxPlayers()));
        set("ingame", Objects.toString(false));
        publish();
    }

    private void set(String field, String value) {
        AWRedis.execute(jedis -> {
            jedis.hset("server:" + servername, field, value);
            return true;
        });
    }

    private void publish() {
        AWRedis.publish("sign.update", servername);
    }

}