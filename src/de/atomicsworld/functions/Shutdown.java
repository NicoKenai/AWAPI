package de.atomicsworld.functions;

import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import de.atomicsworld.util.Actionbar;
import de.atomicsworld.util.TypeUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import java.util.Objects;

import static de.atomicsworld.APIMain.dsp;

public class Shutdown extends AWCommand {

    public Shutdown shutdown;

    @Override
    protected void prepare() {
        permission("shutdown");
        usage = "/shutdown (seconds) (target-server) | -a";
        minArgs(0);
        maxArgs(3);
    }

    @Override
    public void perform(CommandSender p, String[] args) {
        if (args[0].contains("-a")) {
            abort(p);
            return;
        }

        if(shutdown != null){
            p.sendMessage(APIMain.prefix + "Es läuft schon ein Shutdown. Du kannst ihn mit der §e-a Flag§7 abbrechen.");
            return;
        }

        if (args.length == 0) {
            // /shutdown
            shutdown("group:lobby");
            return;
        }

        if (args.length == 1) {

            if (TypeUtils.isInt(args[0])) {
                shutdown(args[0], "group:lobby");
            } else p.sendMessage(APIMain.prefix + "Das erste Argument muss eine §eZahl§7 sein.");
        }

        if (args.length == 2) {
            if (TypeUtils.isInt(args[0])) {
                shutdown(args[0], args[1]);
            } else p.sendMessage(APIMain.prefix + "Das erste Argument muss eine §eZahl§7 sein.");
        }
    }

    public void abort(CommandSender sender) {
        if (shutdown == null) {
            sender.sendMessage(APIMain.prefix + "Zur Zeit läuft kein Shutdown.");
            return;
        } else shutdown = null;
        sender.sendMessage(APIMain.prefix + "Shutdown abgebrochen!");
    }

    public void shutdown(Object seconds, String targetserver) {
        if (Integer.parseInt(seconds.toString()) == 0) {
            shutdown(targetserver);
            return;
        } else shutdown(Integer.parseInt(seconds.toString()), targetserver);
    }

    int c;
    int c2 = 0;

    public void shutdown(int seconds, String target) {
        c2 = seconds;
        shutdown = this;
        c = Bukkit.getScheduler().scheduleSyncRepeatingTask(APIMain.plugin, new Runnable() {
            @Override
            public void run() {
                if (shutdown == null) {
                    Bukkit.getScheduler().cancelTask(c);
                    c2 = 0;
                    return;
                } else {
                    if (c2 > 0) {
                        c2--;
                        if(c2 < 120) Actionbar.online(dsp.get("shutdown.countdown", c2), false);
                    } else if (c2 == 0) shutdown(target);
                }

            }
        }, 0, 20L);
    }


    public void shutdown(String server) {
        Bukkit.getOnlinePlayers().forEach(o -> {
            if (server == "group:lobby") {
                dsp.send(o, "shutdown.kick", "die Lobby");
                AWAPI.sendPlayerToHub(o);
            } else
                dsp.send(o, "shutdown.kick", server);
                AWAPI.sendPlayer(o, server);
        });
        Bukkit.getScheduler().runTaskLater(APIMain.plugin, () -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "stop"), 40);
    }
}
