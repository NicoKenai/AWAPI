package de.atomicsworld.functions;

import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import de.atomicsworld.redis.AWRedis;
import de.atomicsworld.util.RankColor;
import de.atomicsworld.util.TimeUtils;
import net.minecraft.server.v1_11_R1.IChatBaseComponent;
import net.minecraft.server.v1_11_R1.PacketPlayOutPlayerListHeaderFooter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.lang.reflect.Field;

import java.text.SimpleDateFormat;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by Kenai on 08.01.2017.
 */
public class Tablist {

    static String header = "§8« §3§lAtomicsWorld.de§8 »";
    static String footer = "§7Server: §e" + Bukkit.getServerName();
    static String port = "\n§7Port: §e" + Bukkit.getPort();
    static String ranks = "§2Technik §4Support §bCreator\n §5YouTuber §ePremium";

    public Tablist() {
        this.a = sb.getTeam("1a") == null ? sb.registerNewTeam("1a") : sb.getTeam("1a");
        this.b = sb.getTeam("2b") == null ? sb.registerNewTeam("2b") : sb.getTeam("2b");
        this.c = sb.getTeam("3c") == null ? sb.registerNewTeam("3c") : sb.getTeam("3c");
        this.d = sb.getTeam("4d") == null ? sb.registerNewTeam("4d") : sb.getTeam("4d");
        this.e = sb.getTeam("5e") == null ? sb.registerNewTeam("5e") : sb.getTeam("5e");
        this.f = sb.getTeam("6f") == null ? sb.registerNewTeam("6f") : sb.getTeam("6f");
        this.g = sb.getTeam("7g") == null ? sb.registerNewTeam("7g") : sb.getTeam("7g");
        this.h = sb.getTeam("8h") == null ? sb.registerNewTeam("8h") : sb.getTeam("8h");
        this.i = sb.getTeam("9i") == null ? sb.registerNewTeam("9i") : sb.getTeam("9i");
    }

    static Field bfield;

    public static void setTablist(UUID uuid) {
        Player p = Bukkit.getPlayer(uuid);
        CraftPlayer cp = (CraftPlayer) p;

        IChatBaseComponent tabheader;
        if (Bukkit.getPlayer(uuid).hasPermission("aw.tab.serversettings")) {
            tabheader = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + header + port + "\"}");
        } else tabheader = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + header + "\"}");

        IChatBaseComponent tabfooter;
        tabfooter = IChatBaseComponent.ChatSerializer.a("{\"text\" : \"" + ranks + "\n" + footer + "\"}");
        PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter(tabheader);
        try {
            if (bfield == null) {
                bfield = packet.getClass().getDeclaredField("b");
                bfield.setAccessible(true);
            }
            bfield.set(packet, tabfooter);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        cp.getHandle().playerConnection.sendPacket(packet);
    }

    private static final org.bukkit.scoreboard.Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();
    public Team a;
    public Team b;
    public Team c;
    public Team d;
    public Team e;
    public Team f;
    public Team g;
    public Team h;
    public Team i;

    public static void setPlayer(Player p) {
        String team = "";
        team = "";
        if (p.hasPermission("aw.color.admin")) {
            team = "1a";
        } else if (p.hasPermission("aw.color.dev")) {
            team = "2b";
        } else if (p.hasPermission("aw.color.mod")) {
            team = "3c";
        } else if (p.hasPermission("aw.color.sup")) {
            team = "4d";
        } else if (p.hasPermission("aw.color.builder")) {
            team = "5e";
        } else if (p.hasPermission("aw.color.helfer")) {
            team = "6f";
        } else if (p.hasPermission("aw.color.youtuber")) {
            team = "7g";
        } else if (p.hasPermission("aw.color.premium")) {
            team = "8h";
        } else team = "9i";

        String prefix = RankColor.get(p);
        sb.getTeam(team).setPrefix(prefix.replaceAll(prefix.substring(0, 1), ""));
        sb.getTeam(team).addPlayer(Bukkit.getOfflinePlayer(p.getUniqueId()));
        sb.getTeam(team).addEntry(p.getName());

        String name = "";
        if (p.getUniqueId().toString().equals("2209154d-3de3-4872-baa5-6baeef526d1b")) {
            name = "§4§lAtomics§c§lWorld";
        } else name = String.valueOf(sb.getTeam(team).getPrefix()) + p.getName();

        p.setPlayerListName(name);
        p.setScoreboard(sb);
        Bukkit.getScheduler().runTaskTimer(APIMain.plugin, () -> {
            Bukkit.getOnlinePlayers().forEach(pl -> pl.setScoreboard(sb)); //Zwar MEGA unschön aber was willste tun?
        }, 1, 1);
    }

}
