package de.atomicsworld.functions;

import de.atomicsworld.util.command.AtomicsCommand;
import org.bukkit.command.CommandSender;


public class Test {

    @AtomicsCommand(
            name = "rankcolors",
            permissions = {"aw.test"},
            minArgs = 1,
            maxArgs = 1,
            usage = "/rankcolors <1|2>"
    )
    public void exe(CommandSender p, Integer i) {
        switch (i) {
            case 1:
                p.sendMessage(new String("&4Admin &cDev &2Mod&a Sup &bBuilder &eHelfer &5YT &6Premium &7User;" +
                        "&5Admin &9Dev &4Mod &3Sup &bBuilder &cHelfer &5YT &6Premium &7User;" +
                        "&1Admin &2Dev &4Mod &aSup &eBuilder &3Helfer &5YT &6Pro &7User;" +
                        ""
                ).replaceAll("&", "§").replaceAll(";", "\n\n"));
                break;
            case 2:
                p.sendMessage(new String(
                        "&4Technik &r&o(Admin/Dev) &cOrganisation &r&o(Mod) &9Support &r&o(Sup & Helfer) &eArchitekt &r&o(Builder) &5YT &6VIP &7User;" +
                        "&2Technik &r&o(Admin/Dev) &cOrganisation &r&o(Mod) &4Support &r&o(Sup & Helfer) &bArchitekt &r&o(Builder) &5YT &6VIP &3User"
                ).replaceAll("&", "§").replaceAll(";", "\n\n"));
                break;
            default:
                p.sendMessage("/rankcolors 1|2");
        }
    }
}
