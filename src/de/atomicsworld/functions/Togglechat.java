package de.atomicsworld.functions;

import org.bukkit.command.CommandSender;


import static de.atomicsworld.APIMain.prefix;
import static de.atomicsworld.listener.ChatListener.chat;

/**
 * Created by NicoH on 05.12.2016.
 */
public class Togglechat extends AWCommand {

    @Override
    protected void prepare() {
        usage = "/togglechat";
        permission("chat.toggle");
        maxArgs(0);
    }

    @Override
    public void perform(CommandSender sender, String[] args) {
        if(chat){
            sender.sendMessage(prefix+"Der Chat wurde §cde§7aktiviert.");
            chat = false;
        } else {
            chat = true;
            sender.sendMessage(prefix+"Der Chat wurde §aak§7tiviert.");
        }
    }


}
