package de.atomicsworld.functions;

import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import de.atomicsworld.functions.permissions.Perms;
import de.atomicsworld.util.command.AtomicsCommand;
import org.bukkit.command.CommandSender;


public class YoutuberManager {

    @AtomicsCommand(
            name = "youtuber",
            usage = "/youtuber",
            permissions = {"aw.youtuber.manage"}
    )
    public void youtuber(CommandSender player) {
        player.sendMessage(APIMain.prefix + "Benutzung der YouTuber Verwaltung:");
        player.sendMessage(APIMain.symbol + "§e/youtuber add <Name> premium §8 - §6Premium+ §7setzen");
        player.sendMessage(APIMain.symbol + "§e/youtuber add <Name> youtube §8 - §5YouTuber§7 setzen");
        player.sendMessage(APIMain.symbol + "§e/youtuber remove <Name> §8 - §7YouTuber-Ränge entfernen");
    }

    @AtomicsCommand(
            name = "add",
            parent = "youtuber",
            minArgs = 2,
            maxArgs = 2,
            usage = "/youtuber add <Name> <youtube|premium>",
            permissions = {"aw.youtuber.manage"}, runAsync = true
    )
    public void add(CommandSender player, String name, String type) {
        switch (type) {
            case "youtube":
                Perms.addGroup(AWAPI.getUser(name), "youtuber");
                player.sendMessage(APIMain.prefix + "Der Spieler §e" + name + "§7 hat jetzt den §5YouTuber§7-Rang.");
                break;
            case "premium":
                Perms.addGroup(AWAPI.getUser(name), "premiumplus");
                player.sendMessage(APIMain.prefix + "Der Spieler §e" + name + "§7 hat jetzt den §6Premium+§7-Rang.");
                break;
            default:
                player.sendMessage(APIMain.prefix + "Du kannst einen Spieler nur in die §5youtube§7 oder §6premium§7 Gruppe setzen.");
                break;
        }
    }

    @AtomicsCommand(
            name = "remove",
            parent = "youtuber",
            minArgs = 1,
            maxArgs = 1,
            usage = "/youtuber add <Name>",
            permissions = {"aw.youtuber.manage"}, runAsync = true
    )
    public void remove(CommandSender player, String name) {
        Perms.removeGroup(AWAPI.getUser(name), "youtuber");
        Perms.removeGroup(AWAPI.getUser(name), "premiumplus");
        player.sendMessage(APIMain.prefix + "Der Spieler §e" + name + "§7 ist jetzt in keiner YouTuber-Gruppe mehr.");
    }


}
