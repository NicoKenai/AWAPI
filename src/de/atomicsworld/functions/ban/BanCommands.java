package de.atomicsworld.functions.ban;

import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import de.atomicsworld.database.entities.User;
import de.atomicsworld.util.TypeUtils;
import de.atomicsworld.util.command.AtomicsCommand;
import org.bukkit.entity.Player;

import java.text.SimpleDateFormat;
import java.util.UUID;

import static de.atomicsworld.APIMain.dsp;

public class BanCommands {

    SimpleDateFormat timeFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    @AtomicsCommand(
            name = "ban",
            minArgs = 1,
            maxArgs = 1,
            usage = "/ban <Player>",
            runAsync = true
    )
    public void ban(Player sender, String player) {
        if (AWAPI.getUser(player) != null) {
            new BanMenu(sender, AWAPI.getUser(player));
        } else dsp.playerNull(sender, player);
    }

    @AtomicsCommand(
            name = "baninfo",
            minArgs = 1,
            maxArgs = 1,
            usage = "/baninfo <Player>",
            runAsync = true
    )
    public void baninfo(Player sender, String player) {
        if (!(APIMain.dsp.isPlayerNull(sender, player))) {
            User p = AWAPI.getUser(player);
            dsp.send(sender, "baninfo.title", p.getUserName());
            BanUtils.getBans(p.getUuid()).forEach(ban -> dsp.send(sender, "baninfo.info", ban.getId(), timeFormat.format(ban.getBannedDate()), AWAPI.getUser(UUID.fromString(ban.getAuthorUuid())).getUserName(), AWAPI.getUser(UUID.fromString(ban.getUuid())).getUserName(), ban.getReason(), timeFormat.format(ban.getBannedUntil()), "§4-§c-§4-§c-§4-§c-§4-§c-§4-§c-§4-§c-§4-"));
        }
    }


    @AtomicsCommand(
            name = "delban",
            minArgs = 1,
            maxArgs = 1,
            usage = "/delban <ID>",
            runAsync = true
    )
    public void delban(Player sender, Integer id) {
        if (BanUtils.getBan(id) != null) {
            BanUtils.delban(id);
            dsp.send(sender, "ban.deleted", id);
        } else dsp.send(sender, "ban.notfound", id);
    }

    @AtomicsCommand(
            name = "editban",
            minArgs = 3,
            usage = "/editban <ID> <reason|duration (sec)> <value>",
            runAsync = true
    )
    public boolean editban(Player sender, Integer id, String action, String value) {
        if (BanUtils.getBan(id) != null) {
            switch (action.toLowerCase()) {
                case "reason":
                    if (!sender.hasPermission("aw.editban." + "reason")) {
                        dsp.send(sender, "ban.noperm", "Reason");
                        return true;
                    }
                    BanUtils.editBan(id, value);
                    dsp.send(sender, "ban.edited", "Grund", value);
                    return true;
                case "duration":
                    if (!sender.hasPermission("aw.editban." + "duration")) {
                        dsp.send(sender, "ban.noperm", "Duration");
                        return true;
                    }
                    if (TypeUtils.isInt(value)) {
                        Integer duration = Integer.parseInt(value) * 1000;
                        BanUtils.editBan(id, duration);
                        dsp.send(sender, "ban.edited", "Dauer", new SimpleDateFormat("dd.MM.yyyy HH:mm").format(BanUtils.getBan(id).getBannedUntil()));
                    } else {
                        dsp.send(sender, "ban.duration.usage");
                    }
                    return true;
                default:
                    return false;
            }
        } else {
            dsp.send(sender, "ban.notfound", id);
            return true;
        }
    }

    @AtomicsCommand(
            name = "unban",
            minArgs = 1,
            maxArgs = 1,
            usage = "/unban <Player>",
            runAsync = true
    )
    public void unban(Player sender, String player) {
        User u = AWAPI.getUser(player);
        if (u != null) {
            UUID uuid = u.getUuid();
            if (BanUtils.isCurrentBanned(uuid)) {
                BanUtils.unban(BanUtils.getCurrentBanId(uuid).getId());
                dsp.send(sender, "unban.success", u.getUserName());
            } else dsp.send(sender, "ban.notbanned", u.getUserName());
        } else dsp.playerNull(sender, player);
    }
}
