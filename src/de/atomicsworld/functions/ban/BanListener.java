package de.atomicsworld.functions.ban;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;


import static de.atomicsworld.functions.ban.BanUtils.isCurrentBanned;

/**
 * Created by NicoH on 05.12.2016.
 */
public class BanListener implements Listener {

    @EventHandler
    public void onLogin(PlayerLoginEvent e) {
        if (isCurrentBanned(e.getPlayer().getUniqueId())) e.disallow(PlayerLoginEvent.Result.KICK_BANNED, BanUtils.banScreen(e.getPlayer()));
    }
}