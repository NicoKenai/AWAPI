package de.atomicsworld.functions.ban;

import de.atomicsworld.AWAPI;
import de.atomicsworld.database.entities.User;
import de.atomicsworld.util.TimeUtils;
import de.atomicsworld.util.UUIDFetcher;
import de.atomicsworld.util.executor.ThreadExecutor;
import de.atomicsworld.util.menu.MenuItem;
import de.atomicsworld.util.menu.PopupMenu;
import de.atomicsworld.util.slack.SlackChannel;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * Created by NicoH on 06.12.2016.
 */
public class BanMenu extends PopupMenu {

    UUID ban;
    Player v;

    public BanMenu(Player viewer, User ban) {
        super(ban.getUserName(), 2);
        this.ban = ban.getUuid();
        this.v = viewer;

        ThreadExecutor.executeAsync(() -> {
            addItems();
        }).onDone(() -> openMenu(viewer));
    }

    public void addItems() {
        int i = -1;
        for (BanType type : BanType.values()) {
            i++;
            this.addMenuItem(new MenuItem(type.getName(), new ItemStack(type.getMaterial()), "§7Dauer: §e" + TimeUtils.formatTime(type.getBanDuration())) {
                @Override
                public void onClick(Player arg0) {
                    if (type.getPermission() == null) {
                        BanUtils.ban(ban, arg0.getUniqueId(), type);
                        AWAPI.sendSlackMessage(SlackChannel.LOGS, v.getPlayer().getName() + " hat *" + AWAPI.getUser(ban).getNickname() + "* für _" + TimeUtils.formatTime(type.getBanDuration()) + "_ gebannt. Grund: _" + type.getName() + "_.");
                        closeMenu(arg0);
                    } else if (v.hasPermission(type.getPermission())) {
                        BanUtils.ban(ban, arg0.getUniqueId(), type);
                        AWAPI.sendSlackMessage(SlackChannel.LOGS, v.getPlayer().getName() + " hat *" + UUIDFetcher.getName(ban) + "* für _" + TimeUtils.formatTime(type.getBanDuration()) + "_ gebannt. Grund: _" + type.getName() + "_.");
                        closeMenu(arg0);
                    }
                }
            }, i);
        }
    }
}
