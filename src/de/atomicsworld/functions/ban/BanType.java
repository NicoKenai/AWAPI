package de.atomicsworld.functions.ban;

import org.bukkit.Material;

/**
 * Created by NicoH on 05.12.2016.
 */
public enum BanType {

    PERMANENT("Permanent", Material.REDSTONE_BLOCK, "aw.ban.permanent", -1),

    //CHAT
    SPAM("Spam", Material.PAPER, null, 24 * 60 * 60 * 1000),
    BELEIDIGUNG("Beleidigung", Material.TRAPPED_CHEST, null, 2 * 24 * 60 * 60 * 1000),


    WERBUNG("Werbung für YouTube, Twitter, Websites, etc.", Material.RED_SHULKER_BOX, null, 5 * 24 * 60 * 60 * 1000),
    WERBUNG2("Werbung für Minecraft-Server", Material.BLUE_SHULKER_BOX, null, 7 * 24 * 60 * 60 * 1000),
    WERBEBOT("Werbebot", Material.GREEN_SHULKER_BOX, "aw.ban.bot", -1),

    PROVOKATION("Provokation", Material.SKULL_ITEM, null, 12 * 60 * 60 * 1000),
    NAZI("Nationalsozialismus", Material.BLACK_SHULKER_BOX, null, 15 * 24 * 60 * 60 * 1000),
    PRIVAT("Publizieren sensibler (privater) Daten", Material.DIAMOND, null, 15 * 24 * 60 * 60 * 1000),
    UNANGEMESSEN("Unangemessenes Chatverhalten", Material.BOOK, null, 6 * 60 * 60 * 1000),

    //HACKS
    KILLAURA("Killaura", Material.DIAMOND_SWORD, null, 21 * 24 * 60 * 60 * 1000),
    SPEED("Speedhack", Material.GOLD_BOOTS, null, 21 * 24 * 60 * 60 * 1000),
    NOSLOWDOWN("No-Slowdown", Material.SOUL_SAND, null, 21 * 24 * 60 * 60 * 1000),
    FLY("Flyhack", Material.FEATHER, null, 21 * 24 * 60 * 60 * 1000),
    SPIDER("Spider", Material.STRING, null, 21 * 24 * 60 * 60 * 1000),

    //GENERELL
    BUGUSING("Bugusing", Material.LAVA_BUCKET, null, 15 * 24 * 60 * 60 * 1000),;

    private final String name;
    private final Material material;
    private final String permission;
    private final long banDuration;

    BanType(String name, Material material, String permission, long banDuration) {
        this.name = name;
        this.material = material;
        this.permission = permission;
        this.banDuration = banDuration;
    }

    public Material getMaterial() {
        return material;
    }

    public long getBanDuration() {
        return banDuration;
    }

    public String getPermission() {
        return permission;
    }

    public String getName() {
        return name;
    }
}
