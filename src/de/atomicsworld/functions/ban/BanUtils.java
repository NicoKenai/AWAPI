package de.atomicsworld.functions.ban;

import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import de.atomicsworld.database.entities.Bans;
import de.atomicsworld.util.TimeUtils;
import de.atomicsworld.util.executor.ThreadExecutor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;

import static de.atomicsworld.APIMain.dsp;
import static de.atomicsworld.APIMain.prefix;
import static de.atomicsworld.APIMain.symbol;

/**
 * Created by NicoH on 05.12.2016.
 */
public class BanUtils {

    static SimpleDateFormat timeFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    public static void ban(UUID uuid, UUID authoruuid, BanType type) {
        if (!isCurrentBanned(uuid)) {
            ThreadExecutor.executeAsync(() -> {
                Bans b = new Bans(uuid.toString(), type.getName(), authoruuid.toString(), TimeUtils.getCurrentTimestamp().getTime(), type.getBanDuration(), type.getBanDuration() + System.currentTimeMillis());
                APIMain.database.save(b);
            });
        }
        if (type == BanType.PERMANENT && (Bukkit.getPlayer(uuid) != null && Bukkit.getPlayer(uuid).isOnline())) {
            Bukkit.getPlayer(uuid).kickPlayer(dsp.get("ban.permanent.info", true));
            dsp.send(Bukkit.getPlayer(authoruuid), "ban.permanent.success", AWAPI.getUser(uuid));
            return;
        }
        dsp.send(Bukkit.getPlayer(authoruuid), "ban.temporary.success", AWAPI.getUser(uuid).getUserName(), type.getName(), new Timestamp(System.currentTimeMillis() + type.getBanDuration()));
        if (Bukkit.getPlayer(uuid) != null && Bukkit.getPlayer(uuid).isOnline()) check(Bukkit.getPlayer(uuid));
    }

    public static String banScreen(Player p) {
        if (isCurrentBanned(p.getUniqueId())) {
            if (getCurrentBanId(p.getUniqueId()) == null) {
                return dsp.get("ban.permanent.show.info", true);
            } else {
                Bans b = getCurrentBanId(p.getUniqueId());
                return dsp.get("ban.show.info", true, b.getReason(), timeFormat.format(b.getBannedUntil()));
            }
        } else return null;
    }

    public static void check(Player p) {
        if (isCurrentBanned(p.getUniqueId())) {
            if (getCurrentBanId(p.getUniqueId()).getDuration() == -1 || getCurrentBanId(p.getUniqueId()).getDuration() >= 0) {
                Bans b = getCurrentBanId(p.getUniqueId());
                p.kickPlayer(dsp.get("ban.info", true, b.getReason(), timeFormat.format(b.getBannedUntil())));
            }
        }
    }

    public static Bans getCurrentBanId(UUID uuid) {
        List<Bans> z = APIMain.database.find(Bans.class).where().eq("uuid", uuid).findList();
        for (Bans bans : z) if (bans.getBannedUntil() > System.currentTimeMillis()) return bans;
        return null;
    }

    public static List<Bans> getBans(UUID uuid) {
        return APIMain.database.find(Bans.class).where().eq("uuid", uuid).findList();
    }

    public static boolean isCurrentBanned(UUID uuid) {
        for (Bans bans : getBans(uuid)) {
            if (bans.getBannedUntil() > System.currentTimeMillis() || bans.getDuration() == -1) {
                return true;
            }
        }
        return false;
    }

    public static void editBan(Integer banId, String reason) {
        ThreadExecutor.executeAsync(() -> {
            Bans ban = getBan(banId);
            ban.setReason(reason);
            APIMain.database.update(ban);
        });
    }

    public static void editBan(Integer banId, long duration) {
        ThreadExecutor.executeAsync(() -> {
            Bans ban = getBan(banId);
            ban.setDuration(duration);
            ban.setBannedUntil(new Timestamp(System.currentTimeMillis() + duration).getTime());
            APIMain.database.update(ban);
        });
    }

    public static void unban(Integer banId) {
        ThreadExecutor.executeAsync(() -> {
            Bans ban = getBan(banId);
            ban.setBannedUntil(TimeUtils.getCurrentTimestamp().getTime());
            APIMain.database.update(ban);
        });

    }

    public static void delban(Integer banId) {
        ThreadExecutor.executeAsync(() -> APIMain.database.delete(getBan(banId)));
    }

    public static Bans getBan(Integer id) {
        return APIMain.database.find(Bans.class).where().eq("id", id).findUnique();
    }
}
