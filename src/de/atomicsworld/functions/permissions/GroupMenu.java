package de.atomicsworld.functions.permissions;

import de.atomicsworld.APIMain;
import de.atomicsworld.database.entities.User;
import de.atomicsworld.util.ItemStackBuilder;
import de.atomicsworld.util.executor.ThreadExecutor;
import de.atomicsworld.util.menu.MenuItem;
import de.atomicsworld.util.menu.PopupMenu;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;


public class GroupMenu extends PopupMenu {

    public static HashMap<Player, String>  PERMS_ADDPERM_ACTION = new HashMap<>();
    public static HashMap<Player, String> PERMS_REMOVEPERM_ACTION = new HashMap<>();

    public GroupMenu(Player viewer, String group) {
        super("Gruppen Permissions", 1);

        ThreadExecutor.executeAsync(() -> {



            addMenuItem(new MenuItem(group, new ItemStackBuilder(Material.REDSTONE).getItemstack()) {
                @Override
                public void onClick(Player player) {

                }
            }, 0);

            addMenuItem(new MenuItem("§ePermission adden", new ItemStack(Material.WOOD_DOOR)) {
                @Override
                public void onClick(Player player) {
                    closeMenu(player);
                    PERMS_ADDPERM_ACTION.put(player, group);
                    player.sendMessage(APIMain.prefix + "Bitte gebe nun die gewünschte Permission in den Chat ein.");
                }
            }, 2);

            addMenuItem(new MenuItem("§ePermission removen", new ItemStack(Material.IRON_DOOR)) {
                @Override
                public void onClick(Player player) {
                    closeMenu(player);
                    PERMS_REMOVEPERM_ACTION.put(player, group);
                    player.sendMessage(APIMain.prefix + "Bitte gebe nun die gewünschte Permission in den Chat ein.");
                }
            }, 4);
        }).onDone(() -> openMenu(viewer));

    }

}
