package de.atomicsworld.functions.permissions;

import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import de.atomicsworld.database.entities.BungeePerms;
import de.atomicsworld.database.entities.User;
import de.atomicsworld.util.command.AtomicsCommand;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

import static de.atomicsworld.APIMain.dsp;
import static org.bukkit.Bukkit.getServer;

public class Perms {

    @AtomicsCommand(
            name = "reload",
            parent = "perms",
            runAsync = true
    )
    public void reload(Player p) {
        this.reload();
        dsp.send(p, "perms.reload.success");
    }

    @AtomicsCommand(
            name = "group",
            parent = "perms",
            usage = "/perms group <group>",
            maxArgs = 1,
            minArgs = 1,
            runAsync = true
    )
    public void group(Player p, String group) {
        new GroupMenu(p, group);
    }

    @AtomicsCommand(
            name = "user",
            parent = "perms",
            maxArgs = 1,
            minArgs = 1,
            runAsync = true
    )
    public void user(Player p, String user) {
        User u = AWAPI.getUser(user);
        if (u != null) {
            new PermsMenu(p, u);
        } else dsp.playerNull(p, user);
    }

    @AtomicsCommand(
            name = "perms",
            aliases = {"permissions"},
            maxArgs = 0,
            usage = "/perms"
    )
    public void perms(Player p) {
        dsp.send(p, "perms.usage");
    }

    public static List<BungeePerms> getGroups(UUID uuid) {
        return APIMain.database.find(BungeePerms.class).where().eq("name", uuid).eq("type", 0).eq("key", "groups").findList();
    }

    public static List<BungeePerms> getGroups(User user) {
        return getGroups(user.getUuid());
    }

    public static void removeGroup(User u, String groupname) {
        dispatch("bp user " + u.getUserName() + " group remove " + groupname);
        reload();
    }

    public static void addGroup(User u, String groupname) {
        dispatch("bp user " + u.getUserName() + " group add " + groupname);
        reload();
    }

    public static void removePerm(String group, String node){
        dispatch("bp group " + group + " remove " + node);
        reload();
    }

    public static void addPerm(String group, String node){
        dispatch("bp group " + group + " add " + node);
        reload();
    }

    private static void dispatch(String command) {
        getServer().dispatchCommand(Bukkit.getConsoleSender(), command);
        return;
    }

    public static void reload() { //TODO global mit redis
        dispatch("bp reload");
        return;
    }

}