package de.atomicsworld.functions.permissions;


import com.sun.deploy.uitoolkit.impl.awt.AWTGrayBoxPainter;
import com.sun.org.apache.xpath.internal.operations.Bool;
import de.atomicsworld.APIMain;
import de.atomicsworld.database.entities.BungeePerms;
import de.atomicsworld.database.entities.User;
import de.atomicsworld.util.SkullUtils;
import de.atomicsworld.util.executor.ThreadExecutor;
import de.atomicsworld.util.menu.MenuItem;
import de.atomicsworld.util.menu.PopupMenu;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;


public class PermsMenu extends PopupMenu {

    public static HashMap<Player, User> PERMS_ADDGROUP_ACTION = new HashMap<>();
    public static HashMap<Player, User> PERMS_REMOVEGROUP_ACTION = new HashMap<>();

    public PermsMenu(Player viewer, User target) {
        super("User Permissions", 1);

        ThreadExecutor.executeAsync(() -> {

            List<BungeePerms> groups = Perms.getGroups(target);


            addMenuItem(new MenuItem(target.getUserName(), SkullUtils.getPlayerSkull(target.getUserName()), "§eGruppen: \n\n§7" + getGroupList(groups).replaceAll(";", "\n")) {
                @Override
                public void onClick(Player player) {

                }
            }, 0);

            addMenuItem(new MenuItem("§eGruppe adden", new ItemStack(Material.WOOD_DOOR)) {
                @Override
                public void onClick(Player player) {
                    closeMenu(player);
                    PERMS_ADDGROUP_ACTION.put(player, target);
                    player.sendMessage(APIMain.prefix + "Bitte gebe nun die gewünschte Gruppe in den Chat ein.");
                }
            }, 2);

            addMenuItem(new MenuItem("§eGruppe removen", new ItemStack(Material.IRON_DOOR)) {
                @Override
                public void onClick(Player player) {
                    closeMenu(player);
                    PERMS_REMOVEGROUP_ACTION.put(player, target);
                    player.sendMessage(APIMain.prefix + "Bitte gebe nun die gewünschte Gruppe in den Chat ein.");
                }
            }, 4);
        }).onDone(() -> openMenu(viewer));
    }

    String getGroupList(List<BungeePerms> list) {
        StringBuilder stringBuilder = new StringBuilder();
        String CUT = ";";
        list.forEach(g -> stringBuilder.append(g.getValue() + CUT));
        return stringBuilder.toString();
    }

}