package de.atomicsworld.listener;

import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import de.atomicsworld.functions.NewsSystem;
import de.atomicsworld.functions.permissions.Perms;
import de.atomicsworld.util.Actionbar;
import de.atomicsworld.util.RankColor;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import static de.atomicsworld.APIMain.prefix;
import static de.atomicsworld.functions.permissions.GroupMenu.PERMS_ADDPERM_ACTION;
import static de.atomicsworld.functions.permissions.GroupMenu.PERMS_REMOVEPERM_ACTION;
import static de.atomicsworld.functions.permissions.PermsMenu.PERMS_ADDGROUP_ACTION;
import static de.atomicsworld.functions.permissions.PermsMenu.PERMS_REMOVEGROUP_ACTION;

/**
 * Created by NicoH on 05.12.2016.
 */
public class ChatListener implements Listener {

    public static boolean chat = true;

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {

        if (PERMS_ADDGROUP_ACTION.get(e.getPlayer()) != null) {
            e.setCancelled(true);
            if (!e.getMessage().contains(" ")) {
                Perms.addGroup(PERMS_ADDGROUP_ACTION.get(e.getPlayer()), e.getMessage());
                e.getPlayer().sendMessage(APIMain.prefix + String.format("Der Spieler §e%s§7 ist jetzt in der Gruppe §e%s§7.", PERMS_ADDGROUP_ACTION.get(e.getPlayer()).getUserName(), e.getMessage()));
                PERMS_ADDGROUP_ACTION.remove(e.getPlayer());
                return;
            } else {
                e.getPlayer().sendMessage(APIMain.prefix + "Der Gruppenname darf nur ein Wort sein.");
                return;
            }
        }

        if (PERMS_REMOVEGROUP_ACTION.get(e.getPlayer()) != null) {
            e.setCancelled(true);
            if (!e.getMessage().contains(" ")) {
                Perms.removeGroup(PERMS_REMOVEGROUP_ACTION.get(e.getPlayer()), e.getMessage());
                e.getPlayer().sendMessage(APIMain.prefix + String.format("Der Spieler §e%s§7 ist jetzt nicht mehr in der Gruppe §e%s§7.", PERMS_REMOVEGROUP_ACTION.get(e.getPlayer()).getUserName(), e.getMessage()));
                PERMS_REMOVEGROUP_ACTION.remove(e.getPlayer());
                return;
            } else {
                e.getPlayer().sendMessage(APIMain.prefix + "Der Gruppenname darf nur ein Wort sein.");
                return;
            }
        }

        if (PERMS_ADDPERM_ACTION.get(e.getPlayer()) != null) {
            e.setCancelled(true);
            if (!e.getMessage().contains(" ")) {
                Perms.addPerm(PERMS_ADDPERM_ACTION.get(e.getPlayer()), e.getMessage());
                e.getPlayer().sendMessage(APIMain.prefix + String.format("Die Gruppe §e%s§7 hat nun die Permission §e%s§7.", PERMS_ADDPERM_ACTION.get(e.getPlayer()), e.getMessage()));
                PERMS_ADDPERM_ACTION.remove(e.getPlayer());
                return;
            } else {
                e.getPlayer().sendMessage(APIMain.prefix + "Die Permission darf nur ein Wort sein.");
                return;
            }
        }

        if (PERMS_REMOVEPERM_ACTION.get(e.getPlayer()) != null) {
            e.setCancelled(true);
            if (!e.getMessage().contains(" ")) {
                Perms.removePerm(PERMS_REMOVEPERM_ACTION.get(e.getPlayer()), e.getMessage());
                e.getPlayer().sendMessage(APIMain.prefix + String.format("Die Gruppe §e%s§7 hat nun nicht mehr die Permission §e%s§7.", PERMS_REMOVEPERM_ACTION
                        .get(e.getPlayer()), e.getMessage()));
                PERMS_REMOVEPERM_ACTION.remove(e.getPlayer());
                return;
            } else {
                e.getPlayer().sendMessage(APIMain.prefix + "Die Permission darf nur ein Wort sein.");
                return;
            }
        }

        if (NewsSystem.text.get(e.getPlayer()) != null) {
            e.setCancelled(true);
            NewsSystem.text.put(e.getPlayer(), NewsSystem.text.get(e.getPlayer()) + e.getMessage().replaceAll("&", "§").replaceAll(";", "\n").replaceAll("}", ""));
            if (e.getMessage().contains("}")) {
                NewsSystem.send(e.getPlayer());
            } else e.getPlayer().sendMessage(APIMain.prefix + "Nachricht gespeichert.");
        }

        if (!chat && !e.getPlayer().hasPermission("aw.chat.bypass")) {
            e.setCancelled(true);
            e.getPlayer().sendMessage(String.format("%sDer Chat ist aktuell gesperrt.", prefix));
            return;
        }

        if (!e.getPlayer().hasPermission("aw.chat.ignore.percent") && e.getMessage().contains("%")) {
            e.setMessage(e.getMessage().replaceAll("%", ""));
            e.getPlayer().sendMessage(APIMain.prefix + "Da deine Nachricht ein \"§e%§7\" beinhaltet, wurde dieses Zeichen aufgrund einer Sicherheitslücke entfernt.");
        }

        if (e.getPlayer().hasPermission("aw.chat.color")) {
            e.setMessage(ChatColor.translateAlternateColorCodes('&', e.getMessage()));
        }


        if (!chat) {
            Actionbar.send(e.getPlayer(), "§7Der Chat ist aktuell gesperrt, deine Nachricht wurde trotzdem gesendet.");
        }
        String message = "";
        message += RankColor.getChatName(e.getPlayer());
        message += "§8»§7 ";
        message += e.getMessage();

        e.setFormat(message);
    }

}
