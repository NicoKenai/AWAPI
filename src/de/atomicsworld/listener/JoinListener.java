package de.atomicsworld.listener;

import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import de.atomicsworld.ErrorCode;
import de.atomicsworld.database.entities.User;
import de.atomicsworld.functions.JoinMessage;
import de.atomicsworld.functions.Tablist;
import de.atomicsworld.redis.AWRedis;
import de.atomicsworld.util.RankColor;
import de.atomicsworld.util.TimeUtils;
import de.atomicsworld.util.executor.ThreadExecutor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.sql.Timestamp;
import java.util.HashMap;

import static de.atomicsworld.APIMain.prefix;

/**
 * Created by NicoH on 05.12.2016.
 */
public class JoinListener implements Listener {

    public static HashMap<Player, Timestamp> joined = new HashMap<>();

    @EventHandler(priority = EventPriority.MONITOR)
    public void onJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);
        RankColor.get(e.getPlayer());
        JoinMessage.makeAnnoucement(JoinMessage.Action.JOIN, e.getPlayer());
        Tablist.setTablist(e.getPlayer().getUniqueId());
        e.getPlayer().setDisplayName(RankColor.get(e.getPlayer()).replace(RankColor.get(e.getPlayer()).substring(0, 1), ""));
        Tablist.setPlayer(e.getPlayer());
        chatCheck(e.getPlayer());
        APIMain.sendServerInfo();
        ThreadExecutor.executeLater(() -> {
            userIndexEntry(e.getPlayer());
            joined.put(e.getPlayer(), TimeUtils.getCurrentTimestamp());
            nick(AWAPI.getUser(e.getPlayer()), e.getPlayer());
        }, 35);
    }

    private void nick(User u, Player p) {
        ThreadExecutor.executeAsync(() -> {
            if (u.getNickname() != null) {
                String nick = u.getNickname();
                p.setDisplayName("§9§7" + nick);
                p.setPlayerListName("§9§7" + nick);
                p.setCustomName("§9§7" + nick);
                p.setCustomNameVisible(true);
                p.sendMessage(prefix + "Du bist noch als §e" + nick + "§7 genickt.");
            }
        });
    }

    private void userIndexEntry(Player p) {
        ThreadExecutor.executeAsync(() -> {
            if (AWAPI.getUser(p.getUniqueId()) == null) {
                if(AWAPI.getUser(p.getName()) == null){
                    User u = new User();
                    u.setAtomics(0);
                    u.setFirstJoin(TimeUtils.getCurrentTimestamp());
                    u.setUserName(p.getName());
                    u.setUuid(p.getUniqueId());
                    u.setPlaytime(0);
                    u.setKicked(0);
                    u.setLastJoin(TimeUtils.getCurrentTimestamp());
                    APIMain.database.save(u);
                    System.out.println("[AWAPI] Registered new user " + p.getName() + "! (ID: " + u.getUserId() + ")");
                } else p.kickPlayer(ErrorCode.ERROR_1_BUNGEE);
            } else {
                AWRedis.execute(jedis -> {
                    jedis.hset("player:" + p.getUniqueId().toString(), "server", Bukkit.getServerName());
                    jedis.hset("player:" + p.getUniqueId().toString(), "lastjoin", TimeUtils.getCurrentTimestamp().toString());
                    return true;
                });

                User u2 = AWAPI.getUser(p);
                if (p.getName() != u2.getUserName()) {
                    u2.setUserName(p.getName());
                    AWAPI.db.update(u2);
                }
            }
        });
    }


    private void chatCheck(Player p) {
        if (ChatListener.chat == false && p.hasPermission("aw.chat.bypass")) {
            p.sendMessage(prefix + "Der Chat ist momentan gesperrt, " + RankColor.get(p) + "du§7 kannst aber trotzdem schreiben.");
        }
    }
}
