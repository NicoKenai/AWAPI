package de.atomicsworld.listener;

import com.mysql.jdbc.TimeUtil;
import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import de.atomicsworld.database.entities.User;
import de.atomicsworld.functions.JoinMessage;
import de.atomicsworld.redis.AWRedis;
import de.atomicsworld.util.TimeUtils;
import de.atomicsworld.util.executor.ThreadExecutor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.sql.Timestamp;
import java.util.Objects;


/**
 * Created by NicoH on 06.12.2016.
 */
public class QuitListener implements Listener {

    @EventHandler(priority = EventPriority.MONITOR)
    public void onKick(PlayerKickEvent e) {
        e.setLeaveMessage(null);
        JoinMessage.makeAnnoucement(JoinMessage.Action.LEAVE, e.getPlayer());
        Bukkit.getScheduler().runTaskLater(APIMain.plugin, () -> lastQuitEntry(e.getPlayer()), 18);
        APIMain.sendServerInfo();
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onQuit(PlayerQuitEvent e) {
        e.setQuitMessage(null);
        JoinMessage.makeAnnoucement(JoinMessage.Action.LEAVE, e.getPlayer());
        Bukkit.getScheduler().runTaskLater(APIMain.plugin, () -> lastQuitEntry(e.getPlayer()), 18);
        APIMain.sendServerInfo();
    }

    private void lastQuitEntry(Player p) {
        Timestamp playTimeTime = JoinListener.joined.get(p) != null ? JoinListener.joined.get(p) : null;


        ThreadExecutor.executeAsync(() -> {
            User u = AWAPI.getUser(p);
            u.setLastJoin(TimeUtils.getCurrentTimestamp());
            long playtime = u.getPlaytime() + (TimeUtils.getCurrentTimestamp().getTime() - playTimeTime.getTime());
            u.setPlaytime(playtime);

            int atomics = (int) u.getAtomics();
          //  u.setAtomics(atomics + (int) Integer.parseInt(Objects.toString(playTimeTime.getTime())) * ‪1200000‬);

            /*
            u.setAtomics(u.getAtomics() + playtime * 1000 / (2 + 1));
            System.out.println(TimeUtils.formatTime(playtime * 1000 / (2 + 1)));
            u.setAtomics(u.getAtomics() + u.); */
            AWAPI.db.update(u);//TODO WICHTIG
        });

        AWRedis.execute(jedis -> {
            jedis.hdel("player:" + p.getUniqueId().toString(), "server");
            jedis.hdel("player:" + p.getUniqueId().toString(), "lastjoin");
            return true;
        });

        if (JoinListener.joined.get(p) != null == true) JoinListener.joined.remove(p);
    }

}
