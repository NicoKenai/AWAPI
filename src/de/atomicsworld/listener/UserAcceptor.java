package de.atomicsworld.listener;

import de.atomicsworld.AWAPI;
import de.atomicsworld.database.entities.User;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import java.util.ArrayList;
import java.util.Random;

import static de.atomicsworld.APIMain.prefix;
import static de.atomicsworld.APIMain.symbol;


/**
 * Created by Kenai on 08.01.2017.
 */
//premiumkicks
public class UserAcceptor implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onLogin(PlayerLoginEvent e) {
        User u = AWAPI.getUser(e.getPlayer());
        int max = Bukkit.getMaxPlayers();
        int now = Bukkit.getOnlinePlayers().size();

        if (u == null && now >= max) {
            e.disallow(PlayerLoginEvent.Result.KICK_FULL, prefix + "§cDer Server ist voll.");
            return;
        }

        if (now >= max) {
            if (e.getPlayer().hasPermission("aw.cloud.premium")) {
                run(e.getPlayer());
                e.allow();
            } else e.disallow(PlayerLoginEvent.Result.KICK_FULL, prefix + "§cDer Server ist voll.");
        } else e.allow();


    }

    public static void run(Player p) {
        int max = Bukkit.getMaxPlayers();
        ArrayList<Player> array = new ArrayList<>();

        Bukkit.getOnlinePlayers().forEach(op -> array.add(op));
        Random r = new Random(Bukkit.getOnlinePlayers().size());

        Player target = array.get(r.nextInt());
        User target_user = AWAPI.getUser(target);
        if (target_user.getKicked() == 0) {
            if (Bukkit.getPlayer(target_user.getUuid()).hasPermission("aw.cloud.premium")) {
                run(p);
            } else
                Bukkit.getPlayer(target_user.getUuid()).sendMessage(prefix + "§cDu wurdest von einem Premiumspieler gekickt! §7" + symbol + " §cDeine Chancen gekickt zu werden, sind nun aber geringer.");
            AWAPI.sendPlayerToHub(Bukkit.getPlayer(target_user.getUuid()));
            target_user.setKicked(target_user.getKicked() + 1);
            AWAPI.db.update(target_user);
        } else run(p);
        if (Bukkit.getOnlinePlayers().size() >= max) {
            p.sendMessage(prefix + "§cDer Server ist voller Premium-Spielern, sorry!");
            AWAPI.sendPlayerToHub(p);
        }
    }
}
