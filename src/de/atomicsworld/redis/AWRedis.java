package de.atomicsworld.redis;

import com.google.gson.Gson;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisSentinelPool;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class AWRedis {
    protected static Gson gson = new Gson();
    private static String connectionName;
    private static String password;
    private static JedisSentinelPool pool;
    private static SubscriptionHandler handler;

    @Deprecated
    public static void Initialize(IRedisConfiguration configuration) {
        initialize(configuration);
    }

    /**
     * Initializes the AWRedis component.
     *
     * @param configuration
     * @deprecated use prepare() and initialize(AWRedisPoolConfig config) instead
     */
    @Deprecated
    public static void initialize(IRedisConfiguration configuration) {
        password = configuration.getPassword();
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(200);
        AWRedis.pool = new JedisSentinelPool(configuration.getMaster(), configuration.getHosts(), config, configuration.getPassword());
    }

    public static AWRedisPoolConfig prepareConfig() {
        GenericObjectPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(200);
        return new AWRedisPoolConfig(config);
    }

    public static void initialize(AWRedisPoolConfig config) {
        password = config.getPassword();
        AWRedis.pool = new JedisSentinelPool(config.getMaster(), config.getHosts(), config.getPoolConfig(), config.getPassword());
    }

    /**
     * Destroys the {@link JedisSentinelPool} connection
     */
    public static void disable() {
        pool.destroy();
        handler.disable();
    }

    @Deprecated
    public static Object Execute(RedisCaller caller) {
        return execute(caller);
    }

    /**
     * Executes a redis command with a jedis connection from resource pool and returns the result.
     *
     * @param caller The functions that gets called.
     * @return The result of the jedis call. Needs to be casted to the corret response type.
     */
    @Deprecated
    public static Object execute(RedisCaller caller) {
        try (Jedis jedis = pool.getResource()) {
            return caller.Call(jedis);
        }
    }

    /**
     * Executes a redis command with a jedis connection from resource pool and returns the result.
     *
     * @param caller The functions that gets called.
     * @return The result of the jedis call.
     */
    public static <E> E execute(RedisRunnable<E> caller) {
        try (Jedis jedis = pool.getResource()) {
            return caller.call(jedis);
        }
    }

    /**
     * Easy getting of a single value from Redis (for multiple querys, use execute!)
     *
     * @param key The key to get the value from
     * @return value
     */
    public static String get(String key) {
        try (Jedis jedis = pool.getResource()) {
            return jedis.get(key);
        }
    }

    /**
     * Easy getting of a single value from Redis (for multiple querys, use execute!)
     *
     * @param key  The key to get the value from
     * @param hash The hashkey
     * @return value
     */
    public static String hget(String key, String hash) {
        try (Jedis jedis = pool.getResource()) {
            return jedis.hget(key, hash);
        }
    }

    public static SubscriptionHandler getSubscriptionHandler() {
        if (handler == null) {
            handler = new SubscriptionHandler();
        }
        return handler;
    }

    /**
     * Subscribes to a channel and passes messages from the channel to given RedisSubscription.
     * The subscription will be opened in a new Thread.
     *
     * @param channel      Channel to subscribe to
     * @param subscription RedisSubscription to pass Messages to
     */
    @Deprecated
    public static void subscribe(final String channel, final RedisSubscription subscription) {
        redisCustomConnection(jedis -> {
            jedis.subscribe(subscription, channel);
            return null;
        }, () -> subscribe(channel, subscription));
    }

    /**
     * Subscribes to a channel and passes messages from the channel to given RedisSubscription.
     * The subscription will be opened in a new Thread.
     *
     * @param channel      Channel to subscribe to
     * @param subscription RedisSubscription to pass Messages to
     */
    @Deprecated
    public static void psubscribe(final String channel, final RedisSubscription subscription) {
        redisCustomConnection(jedis -> {
            jedis.subscribe(subscription, channel);
            return null;
        }, () -> psubscribe(channel, subscription));
    }

    /**
     * Subscribes to a channel and passes messages from the channel to given Consumer.
     * The subscription will be opened in a new Thread.
     *
     * @param channel      Channel to subscribe to
     * @param subscription Consumer to pass Messages to
     */
    public static void subscribe(final String channel, final Consumer<String> subscription) {
        getSubscriptionHandler().subscribe(channel, subscription);
    }

    /**
     * Subscribes to a channel and passes messages from the channel to given Consumer.
     * The subscription will be opened in a new Thread.
     *
     * @param channel      Channel to subscribe to
     * @param subscription Consumer to pass Messages to
     */
    public static void psubscribe(final String channel, final BiConsumer<String, String> subscription) {
        getSubscriptionHandler().psubscribe(channel, subscription);
    }

    /**
     * Subscribes to a channel and passes messages from the channel to given Consumer.
     * The subscription will be opened in a new Thread.
     *
     * @param channel      Channel to subscribe to
     * @param clazz        Class that the message will be parsed to via {@link Gson#fromJson(String, Class)}
     * @param subscription Consumer to pass Messages to
     */
    public static <E> void subscribe(final String channel, Class<E> clazz, final Consumer<E> subscription) {
        getSubscriptionHandler().subscribe(channel, clazz, subscription);
    }

    /**
     * Subscribes to a channel and passes messages from the channel to given Consumer.
     * The subscription will be opened in a new Thread.
     *
     * @param channel      Channel to subscribe to
     * @param clazz        Class that the message will be parsed to via {@link Gson#fromJson(String, Class)}
     * @param subscription Consumer to pass Messages to
     */
    public static <E> void psubscribe(final String channel, Class<E> clazz, final BiConsumer<String, E> subscription) {
        getSubscriptionHandler().psubscribe(channel, clazz, subscription);
    }

    @Deprecated
    private static void redisCustomConnection(RedisRunnable work, Runnable onDisconnect) {
        new Thread(() -> {
            Jedis jedis = null;
            try {
                jedis = getJedis();
                if (work != null) {
                    work.call(jedis);
                }
            } finally {
                if (jedis != null) {
                    jedis.disconnect();
                }
                if (onDisconnect != null) {
                    onDisconnect.run();
                }
            }
        }).start();
    }

    /**
     * Opens a new Jedis Connection to the current Host Master
     *
     * @return a new Jedis Connection
     */
    protected static Jedis getJedis() {
        HostAndPort master = pool.getCurrentHostMaster();
        Jedis jedis = new Jedis(master.getHost(), master.getPort(), 0);
        if (password != null && !password.isEmpty()) {
            jedis.auth(password);
        }
        if (connectionName != null && !connectionName.isEmpty()) {
            jedis.clientSetname(connectionName);
        }
        return jedis;
    }

    /**
     * Publishes a message to a Channel
     *
     * @param channel Channel where to publish a message in
     * @param message Message which will be published
     */
    public static void publish(String channel, String message) {
        try (Jedis jedis = pool.getResource()) {
            jedis.publish(channel, message);
        }
    }

    /**
     * Converts a message to a JSON format and plublishes it to a Channel
     *
     * @param channel Channel where to publish a message in
     * @param message Message which will be converted to JSON and published
     */
    public static void publish(String channel, Object message) {
        publish(channel, gson.toJson(message));
    }

    /**
     * Converts a message to a JSON format and plublishes it to a Channel
     *
     * @param channel Channel where to publish a message in
     * @param message Message which will be converted to JSON and published
     * @param jedis   Jedis connection to publish with
     */
    public static void publish(String channel, Object message, Jedis jedis) {
        jedis.publish(channel, gson.toJson(message));
    }
}
