package de.atomicsworld.redis;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import java.util.Set;

public class AWRedisPoolConfig {
    private String master;
    private Set<String> hosts;
    private String password;
    private String connectionName;
    private final GenericObjectPoolConfig poolConfig;

    public AWRedisPoolConfig(GenericObjectPoolConfig poolConfig) {
        this.poolConfig = poolConfig;
    }

    public GenericObjectPoolConfig getPoolConfig() {
        return poolConfig;
    }

    public String getMaster() {
        return master;
    }

    public void setMaster(String master) {
        this.master = master;
    }

    public Set<String> getHosts() {
        return hosts;
    }

    public void setHosts(Set<String> hosts) {
        this.hosts = hosts;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConnectionName() {
        return connectionName;
    }

    public void setConnectionName(String connectionName) {
        this.connectionName = connectionName;
    }
}
