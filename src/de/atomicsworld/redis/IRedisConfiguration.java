package de.atomicsworld.redis;

import java.util.Set;

@Deprecated
public interface IRedisConfiguration {
    String getMaster();

    Set<String> getHosts();

    String getPassword();
}
