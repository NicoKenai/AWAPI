package de.atomicsworld.redis;

import redis.clients.jedis.Jedis;

@Deprecated
public abstract class RedisCaller {
    public abstract Object Call(Jedis jedis);
}
