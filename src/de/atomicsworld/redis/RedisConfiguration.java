package de.atomicsworld.redis;


import java.util.Set;

@Deprecated
public class RedisConfiguration implements IRedisConfiguration {
    private final String master;
    private final Set<String> hosts;
    private final String password;

    public RedisConfiguration(String master, Set<String> hosts, String password) {
        this.master = master;
        this.hosts = hosts;
        this.password = password;
    }

    @Override
    public String getMaster() {
        return master;
    }

    @Override
    public Set<String> getHosts() {
        return hosts;
    }

    @Override
    public String getPassword() {
        return password;
    }
}
