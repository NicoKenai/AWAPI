package de.atomicsworld.redis;

import com.google.gson.Gson;

@Deprecated
public abstract class RedisJsonSubscription<G> extends RedisSubscription {
    private static Gson gson = new Gson();
    private Class<G> clazz;

    public RedisJsonSubscription(Class<G> clazz) {
        this.clazz = clazz;
    }

    public abstract void onMessage(G message);

    @Override
    public void onMessage(String message) {
        onMessage(gson.fromJson(message, clazz));
    }
}