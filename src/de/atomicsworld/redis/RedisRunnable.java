package de.atomicsworld.redis;

import redis.clients.jedis.Jedis;

public interface RedisRunnable<E> {
    E call(Jedis jedis);
}
