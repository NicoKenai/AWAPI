package de.atomicsworld.redis;

import redis.clients.jedis.JedisPubSub;

@Deprecated
public abstract class RedisSubscription extends JedisPubSub {
    public abstract void onMessage(String message);

    @Override
    public void onMessage(String channel, String message) {
        try {
            onMessage(message);
        } catch (Exception ex) {
            System.out.println("Error at AWRedis Subscription (" + channel + "):");
            ex.printStackTrace();
        }
    }
}