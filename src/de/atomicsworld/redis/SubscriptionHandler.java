package de.atomicsworld.redis;

import com.google.common.collect.Sets;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class SubscriptionHandler {

    private final AtomicBoolean masterInit = new AtomicBoolean(false);

    private final AtomicReference<CountDownLatch> startChannel = new AtomicReference<>();
    private final AtomicReference<CountDownLatch> startPattern = new AtomicReference<>();

    private final JedisPubSub pubSubAdapterChannel;
    private final JedisPubSub pubSubAdapterPattern;

    private final Set<Subscription<?>> channelSubscriptions;
    private final Set<Subscription<?>> patternSubscriptions;

    SubscriptionHandler() {
        channelSubscriptions = Sets.newConcurrentHashSet();
        patternSubscriptions = Sets.newConcurrentHashSet();
        pubSubAdapterChannel = new JedisPubSub() {
            @Override
            public void onMessage(String channel, String message) {
                handle(channelSubscriptions, channel, message);
            }
        };
        pubSubAdapterPattern = new JedisPubSub() {
            @Override
            public void onPMessage(String pattern, String channel, String message) {
                handle(patternSubscriptions, channel, message);
            }
        };
        startChannel.set(new CountDownLatch(1));
        startPattern.set(new CountDownLatch(1));

        Thread channel = new Thread(() -> {
            while (true) {
                if (startChannel.get() != null) {
                    try {
                        startChannel.get().await();
                    } catch (InterruptedException ignored) {
                    }
                }
                if (channelSubscriptions.isEmpty()) {
                    startChannel.set(new CountDownLatch(1));
                    return;
                }
                try (Jedis jedis = AWRedis.getJedis()) {
                    String[] channels = channelSubscriptions.stream().map(s -> s.channel).toArray(String[]::new);
                    System.out.println("[AWRedis] Start listening for channels (" + channels.length + ")");

                    startChannel.set(null);
                    jedis.subscribe(pubSubAdapterChannel, channels);
                } catch (Exception ex) {
                    System.out.println("[AWRedis-WARN] Error at channel subscription: ");
                    System.out.println("[AWRedis-WARN] numSub: " + channelSubscriptions.size());
                    ex.printStackTrace();
                } finally {
                    startChannel.set(new CountDownLatch(channelSubscriptions.isEmpty() ? 1 : 0));
                    try {
                        Thread.sleep(100); // avoids crazy spamming
                    } catch (InterruptedException ignored) {
                    }
                    System.out.println("[AWRedis] Automatic channel reconnect initiated!"); // while (true) does the job
                }
            }
        });
        channel.setName("AWRedis Channel Subscription");
        Thread pattern = new Thread(() -> {
            while (true) {
                if (startPattern.get() != null) {
                    try {
                        startPattern.get().await();
                    } catch (InterruptedException ignored) {
                    }
                }
                if (patternSubscriptions.isEmpty()) {
                    startPattern.set(new CountDownLatch(1));
                    return;
                }
                try (Jedis jedis = AWRedis.getJedis()) {
                    String[] patterns = patternSubscriptions.stream().map(s -> s.channel).toArray(String[]::new);
                    System.out.println("[AWRedis] Start listening for patterns (" + patterns.length + ")");

                    startPattern.set(null);
                    jedis.psubscribe(pubSubAdapterPattern, patterns);
                } catch (Exception ex) {
                    System.out.println("[AWRedis-WARN] Error at pattern subscription: ");
                    System.out.println("[AWRedis-WARN] numSub: " + patternSubscriptions.size());
                    ex.printStackTrace();
                } finally {
                    startPattern.set(new CountDownLatch(patternSubscriptions.isEmpty() ? 1 : 0));
                    try {
                        Thread.sleep(100); // avoids crazy spamming
                    } catch (InterruptedException ignored) {
                    }
                    System.out.println("[AWRedis] Automatic pattern reconnect initiated!"); // while (true) does the job
                }
            }
        });
        pattern.setName("AWRedis Pattern Subscription");

        channel.start();
        pattern.start();
    }

    public synchronized void run() {
        if (masterInit.getAndSet(true)) {
            return;
        }
        runChannel();
        runPattern();
    }

    private synchronized boolean runChannel() {
        if (channelSubscriptions.isEmpty() || !masterInit.get()) {
            return false;
        }
        if (startChannel.get() == null) {
            return true;
        } else if (startChannel.get().getCount() == 0) {
            return false;
        }
        startChannel.get().countDown();
        return false;
    }

    private synchronized boolean runPattern() {
        if (patternSubscriptions.isEmpty() || !masterInit.get()) {
            return false;
        }
        if (startPattern.get() == null) {
            return true;
        } else if (startPattern.get().getCount() == 0) {
            return false;
        }
        startPattern.get().countDown();
        return false;
    }

    void subscribe(final String channel, final Consumer<String> subscription) {
        Subscription<String> channelSubscription = new Subscription<>(channel, String.class, (channelId, message) -> subscription.accept(message));
        if (channelSubscriptions.add(channelSubscription)) {
            internalSubscribeChannel();
        }
    }

    void psubscribe(final String pattern, final BiConsumer<String, String> subscription) {
        Subscription<String> patternSubscription = new Subscription<>(pattern, String.class, subscription);
        if (patternSubscriptions.add(patternSubscription)) {
            internalSubscribePattern();
        }
    }

    <E> void subscribe(final String channel, Class<E> clazz, final Consumer<E> subscription) {
        Subscription<E> channelSubscription = new Subscription<>(channel, clazz, (channelId, message) -> subscription.accept(message));
        if (channelSubscriptions.add(channelSubscription)) {
            internalSubscribeChannel();
        }
    }

    <E> void psubscribe(final String pattern, Class<E> clazz, final BiConsumer<String, E> subscription) {
        Subscription<E> patternSubscription = new Subscription<>(pattern, clazz, subscription);
        if (patternSubscriptions.add(patternSubscription)) {
            internalSubscribePattern();
        }
    }

    private void handle(Set<Subscription<?>> list, String channel, String message) {
        list.stream().filter(patternSubscription -> patternSubscription.channel.equalsIgnoreCase(channel))
                .forEach(subscription -> {
                    try {
                        subscription.handleIncoming(channel, message);
                    } catch (Exception e) {
                        System.out.println("[AWRedis] Failed to handle Message at channel " + subscription.channel + ", message: " + message);
                        e.printStackTrace();
                    }
                });
    }


    private synchronized void internalSubscribeChannel() {
        if (runChannel()) {
            pubSubAdapterChannel.unsubscribe();
        }
    }

    private synchronized void internalSubscribePattern() {
        if (runPattern()) {
            pubSubAdapterPattern.punsubscribe();
        }
    }

    void disable() {
        pubSubAdapterChannel.unsubscribe();
        pubSubAdapterPattern.punsubscribe();
    }

    private static class Subscription<F> {
        private final String channel;
        private final Class<F> clazz;
        private final BiConsumer<String, F> subscription;

        private Subscription(String channel, Class<F> clazz, BiConsumer<String, F> subscription) {
            this.channel = channel;
            this.clazz = clazz;
            this.subscription = subscription;
        }

        private void handleIncoming(String channel, String message) {
            if (String.class.equals(clazz)) {
                subscription.accept(channel, (F) message);
            } else {
                subscription.accept(channel, AWRedis.gson.fromJson(message, clazz));
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Subscription<?> that = (Subscription<?>) o;

            return channel.equalsIgnoreCase(that.channel);
        }

        @Override
        public int hashCode() {
            return channel.hashCode();
        }
    }
}
