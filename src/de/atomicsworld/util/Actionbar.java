package de.atomicsworld.util;

import de.atomicsworld.APIMain;
import net.minecraft.server.v1_11_R1.IChatBaseComponent;
import net.minecraft.server.v1_11_R1.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

/**
 * Created by NicoH on 06.12.2016.
 */
public class Actionbar {

    public static void online(String message, boolean prefix) {
        Bukkit.getOnlinePlayers().forEach(p -> send(p, prefix ? APIMain.prefix : "" + message));
    }

    public static void online(String message) {
        online(message, false);
    }

    public static void send(Player p, String message, boolean prefix) {
        send(p, prefix ? APIMain.prefix : "" + message);
    }

    public static void send(Player p, String message) {
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + message + "\" }"), (byte) 2));
    }
}