package de.atomicsworld.util;

import org.bukkit.Location;

import static org.bukkit.Bukkit.getServer;

public class LocationUtil {

    public Location convertStringToLocation(String string) {
        String str2loc[] = string.split("\\:");
        Location loc = new Location(getServer().getWorld(str2loc[0]), 0, 0, 0);
        loc.setX(Double.parseDouble(str2loc[1]));
        loc.setY(Double.parseDouble(str2loc[2]));
        loc.setZ(Double.parseDouble(str2loc[3]));
        return loc;

    }

    public String convertLocationToString(Location loc) {
        return loc.getWorld().getName() + ":" + loc.getBlockX() + ":" + loc.getBlockY() + ":" + loc.getBlockZ();
    }

}
