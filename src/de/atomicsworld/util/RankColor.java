package de.atomicsworld.util;

import de.atomicsworld.AWAPI;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;


/**
 * Created by NicoH on 05.12.2016.
 */
public class RankColor {

    //Caching
    private static HashMap<Player, String> rankColor = new HashMap<>();

    //    static String ranks = "§2Technik §4Support §bCreator §5YouTuber §ePremium";

    public static String getChatName(Player p) {
        UUID uuid = p.getUniqueId();
        String color = get(p);

        if (AWAPI.isNicked(uuid)) return "§7" + AWAPI.getNick(uuid);

        switch (color) {
            case "a§2":
                return "§2" + p.getName();
            case "b§2":
                return "§2" + p.getName();
            case "c§4":
                return "§4" + p.getName();
            case "d§4":
                return "§4" + p.getName();
            case "e§b":
                return "§b" + p.getName();
            case "f§4":
                return "§4" + p.getName();
            case "g§5":
                return "§5" + p.getName();
            case "h§e":
                return "§e" + p.getName();
            case "i§7":
                return "§7" + p.getName();
        }
        return "";
    }

    public static String get(Player p) {
        if (rankColor.get(p) == null) {
            //Technik
            if (p.hasPermission("aw.color.admin")) {
                rankColor.put(p, "a§2");
            } else if (p.hasPermission("aw.color.dev")) {
                rankColor.put(p, "b§2");
                //Support
            } else if (p.hasPermission("aw.color.mod")) {
                rankColor.put(p, "c§4");
            } else if (p.hasPermission("aw.color.sup")) {
                rankColor.put(p, "d§4");
                //Creator
            } else if (p.hasPermission("aw.color.builder")) {
                rankColor.put(p, "e§b");
                //Support
            } else if (p.hasPermission("aw.color.helfer")) {
                rankColor.put(p, "f§4");
            } else if (p.hasPermission("aw.color.youtuber")) {
                rankColor.put(p, "g§5");
            } else if (p.hasPermission("aw.color.premium")) {
                rankColor.put(p, "h§e");
            } else {
                rankColor.put(p, "i§7");
            }
            return rankColor.get(p);
        } else return rankColor.get(p);
    }

    public static String get(Player p, boolean prefix) {
        if (prefix) return get(p);
        if (!(AWAPI.isNicked(p.getUniqueId()))) {
            return new StringBuilder(get(p).replaceFirst(get(p).substring(0), "")).append(p.getName()).toString();
        } else
            return new StringBuilder(Objects.toString(ChatColor.COLOR_CHAR + "7")).append(AWAPI.getNick(p.getUniqueId())).toString();
    }
}