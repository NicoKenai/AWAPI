package de.atomicsworld.util;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;

/**
 * Created by NicoH on 06.12.2016.
 */
public class SkullUtils {
    public static ItemStack Item_Skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
    static SkullMeta Meta_Skull = (SkullMeta) Item_Skull.getItemMeta();
    static ArrayList<String> Lore_Skull = new ArrayList<>();

    public static ItemStack getPlayerSkull(String playername) {
        Meta_Skull.setOwner(playername);
        Lore_Skull.clear();
        Lore_Skull.add(null);
        Meta_Skull.setLore(Lore_Skull);
        Meta_Skull.setDisplayName(playername);
        Item_Skull.setItemMeta(Meta_Skull);
        return Item_Skull;
    }

    public static ItemStack getPlayerSkull(Player p) {
        Meta_Skull.setOwner(p.getName());
        Lore_Skull.clear();
        Lore_Skull.add(null);
        Meta_Skull.setLore(Lore_Skull);
        Meta_Skull.setDisplayName(p.getName().toString());
        Item_Skull.setItemMeta(Meta_Skull);
        return Item_Skull;
    }

}
