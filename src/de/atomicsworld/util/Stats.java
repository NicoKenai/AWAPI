package de.atomicsworld.util;

import de.atomicsworld.APIMain;
import de.atomicsworld.database.entities.Statistics;
import de.atomicsworld.database.entities.User;
import de.atomicsworld.util.command.AtomicsCommand;
import de.atomicsworld.util.executor.ThreadExecutor;
import org.bukkit.command.CommandSender;

import java.util.List;

import static de.atomicsworld.APIMain.dsp;
import static de.atomicsworld.APIMain.usage;
import static de.atomicsworld.AWAPI.db;

/**
 * Rufe tolle Statistiken auf :b
 */
public class Stats {

    public static String getTotalPlaytime() {
        List<User> user = db.find(User.class).findList();
        long playtime = 0;

        for (User u : user) playtime += u.getPlaytime();
        return TimeUtils.formatTime(playtime);
    }

    public static double getTotalAtomics() {
        List<User> user = db.find(User.class).findList();
        double atomics = 0;

        for (User u : user) atomics += u.getAtomics();
        return atomics;
    }

    public static int getPlayedGames() {
        int played = 0;
        List<Statistics> list = db.find(Statistics.class).findList();
        for (Statistics statistics : list) {
            if (statistics.getKey().equals("games")) {
                played = TypeUtils.toInt(statistics.getValue());
            }
        }
        return played;
    }

    public static int getTotalUniquePlayer() {
        List<User> user = db.find(User.class).findList();
        return user.size();
    }

    @AtomicsCommand(
            name = "serverstats",
            maxArgs = 1,
            minArgs = 1,
            runAsync = true,
            usage = "/serverstats <playtime, atomics, user, games>"
    )
    public void cmd(CommandSender p, String key) {
        p.sendMessage(APIMain.prefix + "Lade Statistiken...");
        ThreadExecutor.executeAsync(() -> execute(p, key));
    }


    public void execute(CommandSender p, String key) {
        Object result;
        switch (key) {
            case "playtime":
                dsp.send(p, "serverstats.info", key, getTotalPlaytime());
                break;
            case "atomics":
                dsp.send(p, "serverstats.info", key, getTotalAtomics());
                break;
            case "user":
            case "player":
            case "uniqueplayer":
                dsp.send(p, "serverstats.info", key, getTotalUniquePlayer());
                break;
            case "games":
                dsp.send(p, "serverstats.info", key, getPlayedGames());
                break;
            default:
                dsp.send(p, usage + "/serverstats <type>");
                break;
        }
    }

}