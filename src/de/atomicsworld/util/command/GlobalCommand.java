package de.atomicsworld.util.command;

import de.atomicsworld.util.dispatching.Dispatch;

import java.lang.reflect.Method;
import java.util.Arrays;

public abstract class GlobalCommand<S> {
    private CommandSubMap<S> commandMap;
    private Class playerClass;
    private Dispatch<S> dsp;

    public GlobalCommand(AtomicsCommand command, Object function, Method method, Dispatch<S> dsp, Class playerClass) {
        this.commandMap = new CommandSubMap<>(new CommandProcessor<>(this, command, function, method));
        this.dsp = dsp;
        this.playerClass = playerClass;
    }

    public void addSubCommand(AtomicsCommand command, Object function, Method method) {
        commandMap.putCommand(new CommandProcessor<>(this, command, function, method), Arrays.copyOfRange(command.parent(), 1, command.parent().length));
    }

    public void process(S sender, String[] args) {
        commandMap.run(sender, args);
    }

    public Class getPlayerClass() {
        return playerClass;
    }

    public Dispatch<S> getDispatcher() {
        return dsp;
    }

    public abstract boolean checkPermission(S sender, String permission);
}
