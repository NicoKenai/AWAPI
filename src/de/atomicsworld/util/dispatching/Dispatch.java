package de.atomicsworld.util.dispatching;

import de.atomicsworld.APIMain;
import de.atomicsworld.AWAPI;
import org.bukkit.Bukkit;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.logging.Logger;

public abstract class Dispatch<R> {

    private String title;
    private Logger logger;
    private String textcolor;
    private String highlight;
    private String warning;
    private java.util.ResourceBundle bundle = ResourceBundle.msg;

    public Dispatch(String title, Logger logger) {
        this.title = title;
        this.logger = logger;
        this.textcolor = bundle.getString("color.text");
        this.highlight = bundle.getString("color.highlight");
        this.warning = bundle.getString("color.warning");
    }

    public String get(String key, Object... args) {
        return get(key, false, args);
    }

    public String get(String key, Boolean prefix, Object... args) {
        try {
            StringBuilder builder = new StringBuilder();
            if (prefix) builder.append(APIMain.prefix.replace("AtomicsWorld", title));
            builder.append(MessageFormat.format(ResourceBundle.msg.getString(key), args).replaceAll("%r", textcolor).replaceAll("%w", warning).replaceAll("%h", highlight).replaceAll("%z", "\n" + APIMain.symbol + " "));
            return builder.toString();
        } catch (MissingResourceException e) {
            return "Missing resource-key!";
        }
    }


    public void logInfo(String message, Object... args) {
        this.logger.info(this.get(message, args));
    }

    public void logWarning(String message, Object... args) {
        this.logger.warning(this.get(message, args));
    }

    public void logSevere(String message, Object... args) {
        this.logger.severe(this.get(message, args));
    }

    public boolean isPlayerNull(R reciever, String player) {
        if (AWAPI.getUser(player) == null) {
            sendTextMessage(reciever, APIMain.prefix.replace("AtomicsWorld", title) + get("warning.invalidplayer", player));
            return true;
        } else {
            sendTextMessage(reciever, APIMain.prefix.replace("AtomicsWorld", title) + get("warning.playeroffline", player));
            return false;
        }
    }

    public void playerNull(R reciever, String player) {
        if (AWAPI.getUser(player) == null) {
            sendTextMessage(reciever, APIMain.prefix.replace("AtomicsWorld", title) + get("warning.invalidplayer", player));
        } else
            sendTextMessage(reciever, APIMain.prefix.replace("AtomicsWorld", title) + get("warning.playeroffline", player));
    }

    public void certain(String key, String permission, Object... args) {
        Bukkit.getOnlinePlayers().stream().filter(R -> R.hasPermission(permission)).forEach(R -> R.sendMessage(APIMain.prefix.replace("AtomicsWorld", title) + get(key, args)));
    }

    public void online(String key, Object... args) {
        Bukkit.getOnlinePlayers().forEach(R -> R.sendMessage(APIMain.prefix.replace("AtomicsWorld", title) + get(key, args)));
    }

    public void send(R receiver, String key, Object... args) {
        sendTextMessage(receiver, APIMain.prefix.replace("AtomicsWorld", title) + get(key, args));
    }

    public abstract void sendTextMessage(R receiver, String submit);

}