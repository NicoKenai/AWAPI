package de.atomicsworld.util.dispatching;

public enum LogLevel {

    DEBUG,
    INFO,
    WARNING

}
