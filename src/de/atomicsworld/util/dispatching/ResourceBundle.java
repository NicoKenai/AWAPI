package de.atomicsworld.util.dispatching;

public class ResourceBundle {

    public static java.util.ResourceBundle msg;

    public static void loadResourceBundle(String file) {
        msg = java.util.ResourceBundle.getBundle("i18n." + file);
    }

}