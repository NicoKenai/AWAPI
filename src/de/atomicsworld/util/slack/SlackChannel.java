package de.atomicsworld.util.slack;

import de.atomicsworld.APIMain;

public enum SlackChannel {

    SYSADMIN(APIMain.sysadmin, "Root"),
    LOGS(APIMain.logs, "AtomicLog");

    private final SlackApi api;
    private final String name;

    SlackChannel(SlackApi api, String name) {
        this.api = api;
        this.name = name;
    }

    public SlackApi getApi() {
        return api;
    }

    public String getName() {
        return name;
    }
}
