package de.atomicsworld.util.slack;

public class SlackException extends RuntimeException {

    public SlackException(Throwable cause) {
        super(cause);
    }
}
